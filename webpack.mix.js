let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

let
    // Source assets root path
    assetsPath = 'resources/',
    // Compiled & published assets root path
    assetsPublicPath = 'public/',
    npmPath = './node_modules/';

mix
    // Pack custom js to app.js
    .js(assetsPath + 'js/app.js', assetsPublicPath + 'js')

    // Pack custom styles to app.css
    .sass(assetsPath + 'sass/app.scss', assetsPublicPath + 'css')

    // Pack used libs to lib.css
    .styles(
        [
            npmPath + 'bootstrap/dist/css/bootstrap.css',
            npmPath + 'font-awesome/css/font-awesome.css',
            npmPath + 'keen-ui/dist/keen-ui.css'
        ],
        assetsPublicPath + 'css/lib.css'
    )

    // Copy all fonts to public fonts directory
    .copy(
        [
            // Google 'Roboto' font
            assetsPath + 'fonts/Roboto/*.ttf',
            // Twitter Bootstrap
            npmPath + 'bootstrap/dist/fonts/**',
            // Font awesome
            npmPath + 'font-awesome/fonts/**'
        ],
        assetsPublicPath + 'fonts'
    )

    // Copy all images to public images directory
    .copyDirectory(assetsPath + 'img', assetsPublicPath + 'img')

    // Copy all static files to public static directory
    .copyDirectory(assetsPath + 'static', assetsPublicPath + 'static');
