<?php

namespace App\Contracts;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;

/**
 * Call handler interface
 */
interface CallHandler
{
    /**
     * Request handling
     *
     * @param Request $request
     */
    public function handleRequest(Request $request): void;

    /**
     * Response handling
     *
     * @param JsonResponse|Response $response
     */
    public function handleResponse($response): void;

    /**
     * Call debug info
     *
     * @return array
     */
    public function getCallInfo(): array;

    /**
     * Execution runtime in ms
     *
     * @param bool $stopTimer
     * @return float|null
     */
    public function getRuntime(bool $stopTimer = false);

    /**
     * Add error
     *
     * @param mixed $error
     */
    public function addError($error): void;

    /**
     * Errors getter
     *
     * @return array
     */
    public function getErrors(): array;

    /**
     * Request getter
     *
     * @return mixed
     */
    public function getRequest();

    /**
     * Request ID(code, hash,...) getter
     *
     * @return int|string
     */
    public function getRequestId();

    /**
     * Request setter
     *
     * @param mixed $request
     */
    public function setRequest($request): void;

    /**
     * Response getter
     *
     * @return mixed
     */
    public function getResponse();

    /**
     * Response setter
     *
     * @param mixed $response
     */
    public function setResponse($response): void;
}