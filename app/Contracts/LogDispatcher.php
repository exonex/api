<?php

namespace App\Contracts;

/**
 * Log dispatcher interface
 */
interface LogDispatcher
{
    /**
     * All rules getter
     *
     * @return array
     */
    public function getRules(): array;

    /**
     * Active rules getter for current log entry
     *
     * @param $logEntry
     * @return array
     */
    public function getActiveRules($logEntry): array;

    /**
     * Check if we need save current log entry
     *
     * @param $logEntry
     * @return bool
     */
    public function needSave($logEntry): bool;
}