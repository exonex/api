<?php

namespace App\Contracts;

/**
 * Error handler interface
 */
interface ErrorHandler
{
    /**
     * Error handling
     *
     * @param $data
     */
    public function handleError(array $data): void;
}