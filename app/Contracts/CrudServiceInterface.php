<?php

namespace App\Contracts;

use Illuminate\Database\Eloquent\Collection;

use App\Models\Base\Model;

/**
 * Interface of service, realizing CRUD functional
 */
interface CrudServiceInterface
{
    /**
     * Create existing or update new item.
     * In case of update existing item, $data must have 'id' field or something like that
     *
     * @param array $data
     * @return Model
     * @throws \Exception
     */
    public function save(array $data) : Model;

    /**
     * Read item's data
     *
     * @param int|string $id
     * @param bool $asArray
     * @return array|Model
     * @throws \Exception
     */
    public function read($id, bool $asArray = false);

    /**
     * Return items list in attributes or models array
     *
     * @param bool $asArray
     * @param array $fields Selected fields
     * @return array|Collection|Model[]
     * @throws \Exception
     */
    public function list(bool $asArray = false, array $fields = null);

    /**
     * Delete item
     *
     * @param int|string $id
     * @return bool
     * @throws \Exception
     */
    public function delete($id) : bool;
}