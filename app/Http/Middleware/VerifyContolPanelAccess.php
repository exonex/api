<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Web\AuthController;

class VerifyContolPanelAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guest() || !Auth::user()->isAdmin()) {

            return redirect()->route('pages.index')
                ->with('status', AuthController::STATUS_WARNING)
                ->with('message', trans('auth.access_denied'));
        }

        return $next($request);
    }
}
