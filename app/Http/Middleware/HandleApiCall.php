<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\JsonResponse;

use App\Contracts\CallHandler;

/**
 * Class HandleApiCall
 *
 * Logs requests, logs and formats responses
 *
 * @package App\Http\Middleware
 */
class HandleApiCall
{
    /**
     * @var CallHandler
     */
    private $callHandler;

    /**
     * HandleApiCall constructor.
     *
     * @param CallHandler $callHandler
     */
    public function __construct(CallHandler $callHandler)
    {
        $this->callHandler = $callHandler;
    }

    /**
     * Handle an outcoming response.
     *
     * Log request, log and format response
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Log request
        $this->callHandler->handleRequest($request);

        // Handle request
        /** @var JsonResponse $response */
        $response = $next($request);

        // Log response
        $this->callHandler->handleResponse($response);

        // Format response
        return $this->formatResponse($response);
    }

    /**
     * Response formatting
     *
     * @param JsonResponse $response
     * @return JsonResponse
     */
    private function formatResponse($response)
    {
        // Copy http-status into response
        $data = ['status' => $response->getStatusCode()];
        // Add debug info if needed
        if (env('APP_DEBUG')) {
             $data['info'] = $this->callHandler->getCallInfo();
            if (!$response->isOk() && $response->exception) {
                $data['error'] = $response->exception->getMessage();
                $data['exception'] = $response->getData();
            }
        }
        // Wrap data
        if ($response->isOk()) {
            $data['data'] = $response->getData();
        }

        // Update original response data
        return $response->setData($data);
    }
}
