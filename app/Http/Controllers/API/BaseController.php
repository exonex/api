<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Services\Crud\CountryService;

/**
 * Base API controller
 *
 * User: viktor
 * Date: 11/12/18
 * Time: 23:42 PM
 */
class BaseController extends Controller
{
    /**
     * API status
     *
     * @return \Illuminate\Http\Response
     */
    public function getStatus()
    {
        return response()->json('OK');
    }

    /**
     * Countries list
     *
     * @param CountryService $countryService
     * @return \Illuminate\Http\Response
     */
    public function getCountries(CountryService $countryService)
    {
        return response()->json($countryService->list());
    }
}