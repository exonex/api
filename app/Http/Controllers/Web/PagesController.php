<?php

namespace App\Http\Controllers\Web;

use Illuminate\View\View;
use Illuminate\Contracts\View\Factory;

use App\Http\Controllers\Controller;

/**
 * Static web pages controller
 *
 * User: viktor
 * Date: 7/25/18
 * Time: 8:23 PM
 */
class PagesController extends Controller
{
    /**
     * Create a new web pages controller instance.
     */
    public function __construct()
    {
        // For pages we specify all needed middlewares with routes declaration
        //$this->middleware('guest', ['except' => 'getIndex']);
    }

    /**
     * Base app page route (for example)
     *
     * @return Factory|View
     */
    public function getIndex()
    {
        return view('pages.index');
    }
}