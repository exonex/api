<?php

namespace App\Http\Controllers\Web;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\RedirectResponse;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Services\UserService;
use App\Http\Controllers\Controller;

/**
 * Web & socialite authentication controller
 *
 * User: viktor
 * Date: 7/25/18
 * Time: 8:31 PM
 */
class AuthController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Danger controller status
     *
     * @const string
     */
    const STATUS_WARNING = 'danger';

    /**
     * Danger controller status
     *
     * @const string
     */
    const STATUS_SUCCESS = 'success';

    /**
     * Create a new authentication controller instance.
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Форма авторизации
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogin()
    {
        return view('auth.login');
    }

    /**
     * Авторизация
     *
     * @param UserService $userService
     * @return \Illuminate\Http\Response|RedirectResponse
     */
    public function postLogin(UserService $userService)
    {
        try {
            // Авторизуем пользователя через сервис
            $userService->login(request()->all());

        // Ошибка валидации
        } catch (ValidationException $e) {
            return redirect()->back()
                ->withErrors($e->validator->errors())
                ->withInput();

        // Пользователь не найден
        } catch (ModelNotFoundException $e) {
            return redirect()->route('auth.login')
                ->with('status', static::STATUS_WARNING)
                ->with('message', trans('auth.user_not_found'))
                ->withInput();
        }

        // Редиректим пользователя
        return redirect()->intended(route('pages.index'));
    }

    /**
     * Выход из системы
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogout()
    {
        Auth::logout();

        return redirect()->route('auth.login')
            ->with('status', static::STATUS_SUCCESS)
            ->with('message', trans('auth.success_logout'));
    }
}