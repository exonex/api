<?php

namespace App\Http\Controllers\Web;

use Illuminate\View\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\View\Factory;

use App\Http\Controllers\Controller;

/**
 * User controller
 *
 * User: viktor
 * Date: 11/11/18
 * Time: 8:23 PM
 */
class UserController extends Controller
{
    /**
     * Create a new web pages controller instance.
     */
    public function __construct()
    {
        // Auth middleware already added to routes
        //$this->middleware('auth', ['except' => 'getIndex']);
    }

    /**
     * Current authorized user profile
     *
     * @return Factory|View
     */
    public function getProfile()
    {
        return view('user.profile', [
            'user' => Auth::user(),
        ]);
    }

    /**
     *  Current authorized user oauth settings
     *
     * @return \Illuminate\Http\Response
     */
    public function getOauth()
    {
        return view('user.oauth', [
            'user' => Auth::user(),
        ]);
    }
}