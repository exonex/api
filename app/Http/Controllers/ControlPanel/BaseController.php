<?php

namespace App\Http\Controllers\ControlPanel;

use App\Http\Controllers\Controller;

/**
 * Base Control Panel controller
 *
 * User: viktor
 * Date: 11/10/18
 * Time: 13:25 PM
 */
class BaseController extends Controller
{
    /**
     * Danger controller status
     *
     * @const string
     */
    const STATUS_WARNING = 'danger';

    /**
     * Danger controller status
     *
     * @const string
     */
    const STATUS_SUCCESS = 'success';

    /**
     * Control panel main page
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {
        return view('cp.index');
    }
}