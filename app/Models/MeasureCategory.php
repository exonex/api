<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\HasMany;

use App\Models\Base\Model;

/**
 * Группы единиц измерения
 *
 * Class MeasureGroup
 * @property string  $id
 * @property string  $name      Название
 * @property integer $order     Порядок сортировки
 *
 * @property-read HasMany|MeasureUnit[]     $units   Единицы измерения группы
 *
 * @package App\Models
 */
class MeasureCategory extends Model
{
    /** @const ID групп единиц измерений */
    const LENGTH    = 'ln';
    const AREA      = 'sq';
    const VOLUME    = 'vl';
    const WEIGHT    = 'wt';
    const LIGHT     = 'lm';
    const TEMP      = 'tp';
    const SPEED     = 'sp';
    const FREQUENCY = 'fq';
    const TIME      = 'tm';
    const FORCE     = 'fc';
    const ENERGY    = 'en';
    const POWER     = 'pw';
    const PRESSURE  = 'pr';
    const ELECTRIC  = 'el';
    const CURRENCY  = 'cy';
    const DATA      = 'dt';
    const OTHER     = '-';

    /**
     * The table associated with the model
     * @var string
     */
    protected $table = 'measureCategories';

    /**
     * Indicates if the model should be timestamped
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'order',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
    ];

    /**
     * Fields to select in 'read' action
     *
     * @var array
     */
    protected $readFields = ['id', 'name', 'order'];

    /**
     * Fields to select in 'list' action
     *
     * @var array
     */
    protected $listFields = ['id', 'name', 'order'];

    /**
     * Validation fail messages
     *
     * @var array
     */
    protected static $messages = [];

    /**
     * Единицы измерения группы
     *
     * @return HasMany|MeasureUnit[]
     */
    public function units()
    {
        return $this->hasMany(MeasureUnit::class, 'categoryId', 'id');
    }
}
