<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

use App\Models\Base\Model;

/**
 * Модель товара
 *
 * Class ProductModel
 * @property integer $id            ID
 * @property integer $categoryId    ID категории
 * @property integer $brandId       ID бренда
 * @property string  $name          Название
 * @property string  $code          Код (артикул)
 * @property Carbon  $createdAt
 * @property Carbon  $updatedAt
 * @property Carbon  $deletedAt
 *
 * @property-read BelongsTo|ProductCategory     $category   Категория товаров
 * @property-read BelongsTo|ProductBrand        $brand      Бренд товара
 * @property-read HasMany|Product[]             $products   Товары данной модели
 */
class ProductModel extends Model
{
    use SoftDeletes;

    /** @const Максимальное число параметров одного типа */
    const PROPERTIES_COLS_LIMIT = 32;

    /**
     * The table associated with the model
     *
     * @var string
     */
    protected $table = 'productModels';

    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'categoryId',
        'brandId',
        'name',
        'code',
    ];

    /**
     * Категория товара
     *
     * @return BelongsTo|ProductCategory
     */
    public function category()
    {
        return $this->belongsTo(ProductCategory::class, 'categoryId', 'id');
    }

    /**
     * Бренд товара
     *
     * @return BelongsTo|ProductBrand
     */
    public function brand()
    {
        return $this->belongsTo(ProductBrand::class, 'brandId', 'id');
    }

    /**
     * Товары данной модели
     *
     * @return HasMany|Product[]
     */
    public function products()
    {
        return $this->hasMany(Product::class, 'modelId', 'id');
    }
}
