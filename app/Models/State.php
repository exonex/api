<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

use App\Models\Base\Model;

/**
 * Справочник регионов
 * App\Models\State
 *
 * @property integer $id
 * @property string  $name          Название
 * @property string  $countryId     ID страны
 * @property string  $code          Внутренний код
 * @property integer $timeZone      Часовой пояс (разница со временем в Москве)
 * @property integer $order         Порядок сортировки
 *
 * @property-read BelongsTo|Country     $country      Страна
 * @property-read HasMany|User[]        $users        Пользователи региона
 * @property-read HasMany|Shop[]        $shops        Магазины региона
  */
class State extends Model
{
    /**
     * The table associated with the model
     *
     * @var string
     */
    protected $table = 'states';

    /**
     * Indicates if the model should be timestamped
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * validation rules
     *
     * @var array
     */
    protected static $rules = [
        // Save instance
        self::SCENARIO_SAVE => [
            'name'       => 'required|string|max:255',
            'countryId'  => 'required|string|size:2|exists:countries,id',
            'timeZone'   => 'required|integer|min:-24|max:24',
            'code'       => 'string|max:3|nullable',
            'order'      => 'integer|min:0|nullable',
        ],
    ];

    /**
     * Unique fields
     *
     * @var array
     */
    protected static $unique = [
        self::SCENARIO_SAVE => ['name']
    ];

    /**
     * Validation fail messages
     *
     * @var array
     */
    protected static $messages = [];

    /**
     * Страна
     *
     * @return BelongsTo|Country
     */
    public function country()
    {
        return $this->belongsTo(Country::class, 'countryId', 'id');
    }

    /**
     * Users list
     * 
     * @return HasMany|User[]
     */
    public function users()
    {
        return $this->hasMany(User::class, 'stateId', 'id');
    }

    /**
     * Shops in the region
     *
     * @return HasMany|Shop[]
     */
    public function shops()
    {
        return $this->hasMany(Shop::class, 'stateId', 'id');
    }
}
