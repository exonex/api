<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

use App\Models\Base\LogEntry;

/**
 * Errors logs
 *
 * Class LogError
 * @package App\Models
 *
 * @property integer $id
 * @property Carbon  $time
 * @property integer $userId
 * @property integer $requestId
 * @property string  $type
 * @property string  $action
 * @property string  $tag
 * @property string  $message
 * @property string  $data
 * @property integer $size
 * @property string  $file
 * @property integer $line
 * @property string  $trace
 * @property string  $ip
 *
 * @property-read BelongsTo|User       $user    Log entry user
 * @property-read BelongsTo|LogApiCall $request Initial request
 */
class LogError extends LogEntry
{
    /**
     * The table associated with the model
     *
     * @var string
     */
    protected $table = 'logErrors';

    /**
     * User relation
     *
     * @return BelongsTo|LogApiCall
     */
    public function request()
    {
        return $this->belongsTo(static::class, 'requestId', 'id');
    }
}