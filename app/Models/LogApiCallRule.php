<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

use App\Models\Base\Model;

/**
 * API calls save rules
 *
 * Class LogApiCallRule
 * @package App\Models
 *
 * @property integer $id
 * @property string  $userId
 * @property string  $type
 * @property string  $code
 * @property string  $action
 * @property string  $tag
 * @property string  $ip
 * @property integer $save
 * @property string  $comment
 * @property Carbon  $createdAt
 * @property Carbon  $updatedAt
 * @property Carbon  $deletedAt
 *
 * @property-read BelongsTo|User $user Log entry user
 */
class LogApiCallRule extends Model
{
    const ANY = '*';

    /**
     * The table associated with the model
     *
     * @var string
     */
    protected $table = 'logApiCallRules';

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'Y-m-d H:i:s';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['createdAt', 'updatedAt', 'deletedAt'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
}