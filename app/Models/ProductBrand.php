<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

use App\Models\Base\Model;

/**
 * Марка (бренд) товаров
 *
 * Class ProductBrand
 * @property integer $id            ID
 * @property integer $companyId     ID компании
 * @property string  $name          Название
 * @property string  $nameEn        Общепринятое название на английском
 * @property string  $description   Описание
 * @property string  $logo          Официальный логотип
 * @property Carbon  $createdAt
 * @property Carbon  $updatedAt
 * @property Carbon  $deletedAt
 *
 * @property-read BelongsTo|ProductCompany  $company    Компания - производитель
 * @property-read HasMany|ProductModel[]    $models     Модели товаров бренда
 *
 */
class ProductBrand extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model
     *
     * @var string
     */
    protected $table = 'productBrands';

    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'companyId',
        'name',
        'nameEn',
        'description',
        'logo',
        'order',
    ];

    /**
     * validation rules
     *
     * @var array
     */
    protected static $rules = [
        // Create new instance
        self::SCENARIO_SAVE => [
            'companyId'   => 'required|integer|exists:productCompanies,id',
            'name'        => 'required|string|max:255',
            'nameEn'      => 'required|string|max:255',
            'description' => 'string|max:255',
            'logo'        => 'string|max:255',
            'order'       => 'integer|nullable',
        ],
    ];

    /**
     * Unique fields
     *
     * @var array
     */
    protected static $unique = [
        self::SCENARIO_SAVE => ['name', 'nameEn'],
    ];

    /**
     * Validation fail messages
     *
     * @var array
     */
    protected static $messages = [];

    /**
     * Компания - производитель
     *
     * @return BelongsTo|ProductCompany
     */
    public function company()
    {
        return $this->belongsTo(ProductCompany::class, 'companyId', 'id');
    }

    /**
     * Модели товаров данного бренда
     *
     * @return HasMany|ProductModel[]
     */
    public function models()
    {
        return $this->hasMany(ProductModel::class, 'modelId', 'id');
    }
}
