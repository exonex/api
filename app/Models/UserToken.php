<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Traits\Age;
use App\Models\Base\Model;
use App\Exceptions\TokenExpiredException;

/**
 * Модель токенов сброса пароля и активации пользователя
 * App\Models\PasswordReset
 *
 * @property integer $id
 * @property string  $email         Почта
 * @property string  $token         Токен
 * @property integer $type          Тип токена: 0 - сброс пароля, 1 - активация пользователя (проверка почты)
 * @property Carbon  $updatedAt
 * @property Carbon  $createdAt
 *
 * @property-read User $user        Пользователь
 */
class UserToken extends Model
{
    use Age;

    /** @const Токены восстановления пароля */
    const TYPE_RESET_PASSWORD = 0;
    /** @const Токены активации пользователя */
    const TYPE_ACTIVATE = 1;
    /** @const Токены изменения email */
    const TYPE_CHANGE_EMAIL = 2;

    /** @var array Связь срока жизни токена в часах для разных типов токенов */
    protected static $lifetime = [
        self::TYPE_RESET_PASSWORD => 4,
        self::TYPE_CHANGE_EMAIL   => 4,
        self::TYPE_ACTIVATE       => 720, // 30 дней
    ];

    /**
     * The table associated with the model
     * 
     * @var string
     */
    protected $table = 'userTokens';

    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'token',
        'type',
    ];

    /**
     * Пользователь
     *
     * @return BelongsTo|User
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'email', 'email');
    }

    /**
     * Генерация нового токена
     *
     * @return string
     */
    protected function refreshToken()
    {
        return $this->token = sha1(mt_rand(1000000, 9999999) . $this->email . $this->createdAt . $this->type . "Yippee-ki-yay, motherfucker!");
    }

    /**
     * Добавление нового токена
     *
     * @param string $email Почта пользователя
     * @param int $type     Тип токена
     * @return UserToken|false
     */
    protected static function add(string $email, int $type)
    {
        $attempt = new static();
        $attempt->email = $email;
        $attempt->type = $type;
        $attempt->refreshToken();
        return $attempt->save() ? $attempt : false;
    }

    /**
     * Создание токена сброса пароля
     *
     * @param string $email
     * @return UserToken|false
     */
    public static function createResetPassword(string $email)
    {
        return self::add($email, self::TYPE_RESET_PASSWORD);
    }

    /**
     * Создание токена активации пользователя
     *
     * @param string $email
     * @return UserToken|false
     */
    public static function createActivate(string $email)
    {
        return self::add($email, self::TYPE_ACTIVATE);
    }
    
    /**
     * Создание токена изменения email пользователя
     *
     * @param string $oldEmail
     * @return UserToken|false
     */
    public static function createChangeEmail(string $oldEmail)
    {
        return self::add($oldEmail, self::TYPE_ACTIVATE);
    }

    /**
     * Поиск токена активации пользователя
     * В случае неудачи бросает исключение ModelNotFoundException.
     *
     * @param string $strToken
     * @return UserToken
     * @throws ModelNotFoundException
     */
    public static function findActivate(string $strToken)
    {
        // Токен регистрации срока жизни не имеет, поэтому проверку не выполняем
        return self::where([
                ['token', '=', $strToken],
                ['type', '=', self::TYPE_ACTIVATE]
            ])
            ->orderBy('id', 'desc')
            ->firstOrFail();
    }

    /**
     * Поиск не просроченного токена сброса пароля пользователя
     * В случае неудачи бросает исключение ModelNotFoundException.
     * Если токен найден, но просрочен, бросается исключение TokenExpiredException
     *
     * @param string $strToken
     * @return UserToken
     * @throws ModelNotFoundException
     * @throws TokenExpiredException
     */
    public static function findResetPassword(string $strToken)
    {
        /** @var static $token */
        $token = self::where([
                ['token', '=', $strToken],
                ['type', '=', self::TYPE_RESET_PASSWORD]
            ])
            ->orderBy('id', 'desc')
            ->firstOrFail();
        if ($token->isExpired()) {
            throw new TokenExpiredException();
        }
        return $token;
    }
    
    /**
     * Поиск не просроченного токена изменения email пользователя
     * В случае неудачи бросает исключение ModelNotFoundException.
     * Если токен найден, но просрочен, бросается исключение TokenExpiredException
     *
     * @param string $strToken
     * @return UserToken
     * @throws ModelNotFoundException
     * @throws TokenExpiredException
     */
    public static function findChangeEmail(string $strToken)
    {
        /** @var static $token */
        $token = self::where([
                ['token', '=', $strToken],
                ['type', '=', self::TYPE_RESET_PASSWORD]
            ])
            ->orderBy('id', 'desc')
            ->firstOrFail();
        if ($token->isExpired()) {
            throw new TokenExpiredException();
        }
        return $token;
    }

    /**
     * Проверка срока жизни токена
     *
     * @return bool
     */
    public function isExpired()
    {
        $lifetime = $this->getLifetime();

        return $lifetime && ($lifetime < $this->ageInHours());
    }

    /**
     * Получение срока жизни токена в часах
     *
     * @return int|null
     */
    public function getLifetime()
    {
        return isset($this->type) && isset(self::$lifetime[$this->type]) ? self::$lifetime[$this->type] : null;
    }
}