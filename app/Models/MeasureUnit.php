<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

use App\Models\Base\Model;

/**
 * Единица измерения
 *
 * Class MeasureUnit
 * @property string  $id            Строковый ID (международное обозначение)
 * @property string  $categoryId    ID категории единиц измерения
 * @property string  $name          Название
 * @property string  $designation   Обозначение
 * @property string  $code          Международный код
 * @property string  $codeStr       Международный строковый код
 * @property string  $parentId      Базовая единица измерения
 * @property float   $ratio         Коэффициент отношения с базовой единицей измерения
 * @property integer $order         Порядок сортировки
 *
 * @property-read BelongsTo|MeasureCategory     $category   Группа единиц измерения
 * @property-read BelongsTo|MeasureUnit         $parent     Базовая единица измерения (если есть)
 *
 * @package App\Models
 */
class MeasureUnit extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model
     * @var string
     */
    protected $table = 'measureUnits';

    /**
     * Indicates if the model should be timestamped
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'categoryId',
        'name',
        'designation',
        'code',
        'codeStr',
        'order',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
    ];

    /**
     * validation rules
     *
     * @var array
     */
    protected static $rules = [
        // Save instance
        self::SCENARIO_SAVE => [
            'id'          => 'required|string|max:8',
            'categoryId'  => 'required|string|max:8|exists:measureCategories,id',
            'name'        => 'required|string|max:255',
            'designation' => 'required|string|max:8',
            'code'        => 'string|max:3',
            'codeStr'     => 'string|max:3',
            'parentId'    => 'string|max:255|exists:measureUnits,id',
            'ratio'       => 'numeric',
            'order'       => 'integer|nullable',
        ],
    ];

    /**
     * Unique fields
     *
     * @var array
     */
    protected static $unique = [
        self::SCENARIO_SAVE => ['id', 'name', 'designation'],
    ];

    /**
     * Validation fail messages
     *
     * @var array
     */
    protected static $messages = [];

    /**
     * Группа единиц измерения
     *
     * @return BelongsTo|MeasureCategory
     */
    public function category()
    {
        return $this->belongsTo(MeasureCategory::class, 'categoryId', 'id');
    }

    /**
     * Базовая единица измерения
     *
     * @return BelongsTo|null
     */
    public function parent()
    {
        return $this->belongsTo(static::class, 'parentId', 'id');
    }
}
