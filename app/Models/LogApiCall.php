<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

use App\Models\Base\LogEntry;

/**
 * API calls logs
 *
 * Class LogApiCall
 * @package App\Models
 *
 * @property integer $id
 * @property Carbon  $time
 * @property integer $userId
 * @property integer $requestId
 * @property string  $type
 * @property string  $code
 * @property string  $action
 * @property string  $tag
 * @property string  $data
 * @property integer $size
 * @property float   $runtime
 * @property string  $ip
 *
 * @property-read BelongsTo|User       $user    Log entry user
 * @property-read BelongsTo|LogApiCall $request Initial request (for responses only)
 */
class LogApiCall extends LogEntry
{
    /**
     * Requests
     *
     * @const string
     */
    const TYPE_REQUEST = 'RQ';

    /**
     * Responses
     *
     * @const string
     */
    const TYPE_RESPONSE = 'RS';

    /**
     * The table associated with the model
     *
     * @var string
     */
    protected $table = 'logApiCalls';

    /**
     * User relation
     *
     * @return BelongsTo|LogApiCall
     */
    public function request()
    {
        return $this->belongsTo(static::class, 'requestId', 'id');
    }

    /**
     * Is-request flag getter
     *
     * @return bool
     */
    protected function isRequest()
    {
        return $this->type == static::TYPE_REQUEST;
    }

    /**
     * Is-response flag getter
     *
     * @return bool
     */
    protected function isResponse()
    {
        return $this->type == static::TYPE_RESPONSE;
    }

    /**
     * Request log entry constructor
     *
     * @param array $data
     * @return static
     */
    public static function createRequestLog(array $data)
    {
        $data['type'] = static::TYPE_REQUEST;

        return new static($data);
    }

    /**
     * Response log entry constructor
     *
     * @param array $data
     * @return static
     */
    public static function createResponseLog(array $data)
    {
        $data['type'] = static::TYPE_RESPONSE;

        return new static($data);
    }
}