<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

use App\Models\Base\Model;

/**
 * Производитель товаров
 *
 * Class ProductBrand
 * @property integer $id            ID
 * @property string  $countryId     ID страны
 * @property string  $name          Название
 * @property string  $nameEn        Общепринятое название на английском
 * @property string  $description   Описание
 * @property string  $logo          Официальный логотип
 * @property string  $site          Официальный сайт
 * @property integer $order         Порядок сортировки
 * @property Carbon  $createdAt
 * @property Carbon  $updatedAt
 * @property Carbon  $deletedAt
 *
 * @property-read BelongsTo|Country         $country    Страна
 * @property-read HasMany|ProductBrand[]    $brands     Бренды компании
 */
class ProductCompany extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model
     *
     * @var string
     */
    protected $table = 'productCompanies';

    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'countryId',
        'name',
        'nameEn',
        'description',
        'logo',
        'site',
        'order',
    ];

    /**
     * validation rules
     *
     * @var array
     */
    protected static $rules = [
        // Save instance
        self::SCENARIO_SAVE => [
            'countryId'   => 'required|string|size:2|exists:countries,id',
            'name'        => 'required|string|max:255',
            'nameEn'      => 'required|string|max:255',
            'description' => 'string|max:255',
            'logo'        => 'string|max:255',
            'site'        => 'string|max:255',
            'order'       => 'integer|nullable',
        ],
    ];

    /**
     * Unique fields
     *
     * @var array
     */
    protected static $unique = [
        self::SCENARIO_SAVE => ['name', 'nameEn']
    ];

    /**
     * Validation fail messages
     *
     * @var array
     */
    protected static $messages = [];

    /**
     * Страна
     *
     * @return BelongsTo|Country
     */
    public function country()
    {
        return $this->belongsTo(Country::class, 'countryId', 'id');
    }

    /**
     * Бренды компании
     *
     * @return HasMany|ProductBrand[]
     */
    public function brands()
    {
        return $this->hasMany(ProductBrand::class, 'companyId', 'id');
    }

    /**
     * Поиск компании по названию
     *
     * @param string $name
     * @return Builder|null
     */
    public static function whereName($name)
    {
        return static::where('name', '=', $name);
    }
}
