<?php

namespace App\Models\Base;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

use App\Models\User;
use Illuminate\Support\Facades\Auth;

/**
 * Base LogEntry model parent
 *
 * Class BaseLogEntry
 * @package App\Models\Base
 *
 * @property integer $id
 * @property Carbon  $time
 * @property integer $userId
 * @property integer $requestId
 * @property string  $type
 * @property string  $action
 * @property string  $data
 * @property integer $size
 * @property string  $ip
 *
 * @property-read BelongsTo|User $user Log entry user
 */
abstract class LogEntry extends Model
{
    /**
     * The table associated with the model
     *
     * @var string
     */
    protected $table = '-';

    /**
     * The attributes that should be mutated to dates
     *
     * @var array
     */
    protected $dates = [
        'time',
    ];

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'Y-m-d H:i:s';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['time', 'userId', 'action', 'size', 'ip'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * BaseLogEntry constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->userId = Auth::guest() ? null : Auth::user()->id;
        $this->time = new Carbon();
        $this->ip = request()->ip();
    }

    /**
     * User relation
     *
     * @return BelongsTo|User
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'userId', 'id');
    }

    /**
     * Model pre-save callback
     *
     * @return bool
     */
    protected function beforeSave()
    {
        // Prepare 'data' and 'size' fields
        if (is_array($this->data) || is_object($this->data)) {
            $this->data = json_encode($this->data, JSON_UNESCAPED_UNICODE);
        } elseif ($this->data instanceof \SimpleXMLElement) {
            $this->data = $this->data->asXML();
        }
        $this->updateDataSizeField();
        // Get action
        $this->action = request()->route()
            ? request()->route()->getName()
            : null;

        return true;
    }

    /**
     * Data size field update
     */
    private function updateDataSizeField()
    {
        $this->size = strlen($this->data);
    }

    /**
     * Data updater
     *
     * @param string $key
     * @param mixed $value
     */
    public function __set($key, $value)
    {
        parent::__set($key, $value);
        if ($key == 'data') {
            $this->updateDataSizeField();
        }
    }
}