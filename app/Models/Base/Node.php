<?php

namespace App\Models\Base;

use Baum\Node as BaumNode;

/**
 * Base Baum Model parent class
 *
 * Class BaseNode
 * @package App\Models\Base\
 *
 * @see https://github.com/etrepat/baum
 */
class Node extends BaumNode
{
    use ExtendedModel;

    /**
     * Column name to store the reference to parent's node.
     *
     * @var string
     */
    protected $parentColumn = 'parentId';

    /**
     * Column name for left index.
     *
     * @var string
     */
    protected $leftColumn = 'nsLeft';

    /**
     * Column name for right index.
     *
     * @var string
     */
    protected $rightColumn = 'nsRight';

    /**
     * Column name for depth field.
     *
     * @var string
     */
    protected $depthColumn = 'nsDepth';

    /**
     * The attributes that should be hidden for arrays
     *
     * @var array
     */
    protected $hidden = ['nsLeft', 'nsRight', 'nsDepth'];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['nsLeft', 'nsRight', 'nsDepth'];

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'createdAt';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'updatedAt';

    /**
     * The name of the "deleted at" column.
     *
     * @var string
     */
    const DELETED_AT = 'deletedAt';

    /**
     * Common create and update scenario
     *
     * @const string
     */
    const SCENARIO_SAVE = 'save';

    /**
     * Nodes path and pathName separator
     *
     * @const string
     */
    const PATH_SEPARATOR = '/';
}