<?php

namespace App\Models\Base;

use Illuminate\Support\Arr;
use Illuminate\Support\MessageBag;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Trait ModelMethods
 *
 * @package App\Models\Base
 */
trait ExtendedModel
{
    /**
     * Validation rules (may depends on scenario)
     *
     * @var array
     */
    protected static $rules = [];

    /**
     * Unique fields (may depends on scenario)
     *
     * @var array
     */
    protected static $unique = [];

    /**
     * Validation messages
     *
     * @var array
     */
    protected static $messages = [];

    /**
     * Validation & save scenario
     *
     * @var string
     */
    protected $scenario;

    /**
     * Inner model validator
     *
     * @var \Illuminate\Validation\Validator
     */
    protected $validator;

    /**
     * Model constructor / DB loader
     *
     * @param mixed     $key      Item's PK or specified fields's value
     * @param string    $field    Field name
     * @return static|Model
     * @throws ModelNotFoundException
     */
    public static function model($key = null, $field = null)
    {
        /** @var Model $model */
        $model = new static();
        if ($key !== null) {

            return $model->where($field ?: $model->getKeyName(), '=', $key)->firstOrFail();
        }
        if ($key !== null && $field) {
            $model->$field = $key;
        }

        return $model;
    }

    /**
     * Scenario setter
     *
     * @param string $newScenario
     * @return $this
     */
    public function setScenario($newScenario)
    {
        $this->scenario = $newScenario;

        return $this;
    }

    /**
     * Scenario getter
     *
     * @return string
     */
    public function getScenario()
    {
        return $this->scenario;
    }

    /**
     * Save the model to DB
     * Executes beforeSave(), validate() if specified and afterSave() methods
     *
     * @param  array $options - Optional save settings
     * [
     *      'touch'    => bool(=true),       // Change 'updatedAt' field,
     *      'scenario' => string(=null),     // New model scenario
     *      'validate' => array|bool(=true), // Validation flag or validate fields list
     * ]
     * @return bool
     */
    public function save(array $options = [])
    {
        // Check custom provided scenario
        if ($scenario = Arr::get($options, 'scenario', null)) {
            $this->setScenario($scenario);
        }
        // Before save callback
        if (!$this->beforeSave()) {

            return false;
        }
        // Validate if needed
        if ($validate = Arr::get($options, 'validate', false)) { // (bool)true/false or attributes array
            if (!$this->validate(is_array($validate) ? $validate : [])) {

                return false;
            }
        }
        // Save typical Eloquent model
        if (!parent::save($options)) {

            return false;
        }
        // After save callback
        $this->afterSave();

        return true;
    }

    /**
     * Model attributes validation
     *
     * @param array|string[] $customAttributes
     * @return bool
     * @throws \Exception
     */
    public function validate(array $customAttributes = [])
    {
        $attributes = $this->attributesToArray();
        // Check custom attributes for validate
        if ($customAttributes) {
            $attributes = Arr::only($attributes, $customAttributes);
        }

        return $this->passes(
            $attributes,
            $this->getScenario(),
            $this->getKey() ?: null,
            $customAttributes
        );
    }

    /**
     * Validation messages
     *
     * @return MessageBag
     */
    public function errors()
    {
        return $this->validator ? $this->validator->errors() : new MessageBag();
    }

    // Protected methods

    /**
     * Model pre-save callback
     *
     * @return bool
     */
    protected function beforeSave()
    {
        return true;
    }

    /**
     * Model save callback
     */
    protected function afterSave()
    {
        // Place code here
    }

    /**
     * Validation rules according to scenario
     *
     * @param string $scenario
     * @return array
     * @throws \Exception
     */
    protected function getRules($scenario = null)
    {
        return $this->getScenarioFields($scenario, 'rules');
    }

    /**
     * Unique fields according to scenario
     *
     * @param string $scenario
     * @return array
     * @throws \Exception
     */
    protected function getUniqueFields($scenario = null)
    {
        return $this->getScenarioFields($scenario, 'unique');
    }

    /**
     * Validate $data according to specified $scenario, $exceptId and $customAttributes
     *
     * @param array $data
     * @param string $scenario
     * @param int|string $exceptId
     * @param array $customAttributes
     * @return bool
     */
    protected function passes(array $data = [], string $scenario = null, $exceptId = null, array $customAttributes = []): bool
    {
        if (!$rules = $this->getRules($scenario)) {

            return true;
        }
        $this->validator = Validator::make(
            $data ?: $this->getAttributes(),
            $rules,
            static::$messages,
            $customAttributes
        );
        if (!$this->validator->passes()) {

            return false;
        }

        // After passing all validation rules, check unique fields
        return $this->passesUniqueFields($data, $scenario, $exceptId);
    }

    /**
     * Validate $data according to specified $scenario, $exceptId and $customAttributes
     * and throw ValidationException in case of fail
     *
     * @param array $data
     * @param string $scenario
     * @param mixed $exceptId
     * @param array $customAttributes
     * @throws ValidationException
     */
    protected function passesOrFail(array $data = [], string $scenario = null, $exceptId = null, array $customAttributes = [])
    {
        if (!$this->passes($data, $scenario, $exceptId, $customAttributes)) {

            $this->failValidation();
        }
    }

    /**
     * Unique fields validation
     *
     * @param array $attributes
     * @param string $scenario
     * @param string|int $exceptId
     * @return bool
     */
    protected function passesUniqueFields(array $attributes, $scenario = null, $exceptId = null): bool
    {
        // Check if validating data has no unique fields
        $uniqueFields = static::getUniqueFields($scenario);
        if (!$unique = Arr::only($uniqueFields, array_keys($attributes))) {

            return true;
        }
        /** @var Model $model */
        $model = static::model()->where(
            function ($query) use ($unique, $attributes) {
                /** @var Builder $query */
                foreach ($unique as $field) {
                    // Skip allowed null unique-fields
                    if (isset($attributes[$field]) && $attributes[$field] !== null) {
                        $query = $query->orWhere($field, $attributes[$field]);
                    }
                }
            }
        );
        // Check if except ID (or IDs array) specified
        if ($exceptId !== null) {
            $model = $model->where('id', '<>', $exceptId);
        }
        $isPassed = $model->count() === 0;
        if (!$isPassed) {
            $this->errors()->add('__unique', 'Unique fields [' . implode(', ', $uniqueFields) . '] validation failed');
        }

        return $isPassed;
    }

    /**
     * One field validation
     * Constant static::SCENARIO_$FIELD must be defined
     * Returns true or throws ValidationException
     *
     * @param string $field
     * @param mixed $value
     * @param mixed $exceptId
     * @return bool
     * @throws \Exception
     */
    protected function passesField(string $field, $value, $exceptId = null): bool
    {
        $scenario = constant(static::class . '::SCENARIO_' . strtoupper($field));
        if ($scenario === null) {

            throw new \Exception("'$field' field validation scenario '". get_called_class() . "::SCENARIO_" . strtoupper($field) . "' not found");
        }

        return $this->passes([$field => $value], $scenario, $exceptId);
    }

    /**
     * One field validation or fail
     * Constant static::SCENARIO_$FIELD must be defined
     * Returns void or throws ValidationException
     *
     * @param string $field
     * @param mixed $value
     * @param mixed $exceptId
     * @return void
     * @throws \Exception
     * @throws ValidationException
     */
    protected function passesOrFailField(string $field, $value, $exceptId = null): void
    {
        if (!$this->passesField($field, $value, $exceptId)) {

            $this->failValidation();
        }
    }

    /**
     * Validation fail
     *
     * @throws ValidationException
     */
    protected function failValidation(): void
    {
        throw new ValidationException($this->validator);
    }

    // Private methods

    /**
     * Get model fields list according to scenario and list name
     *
     * @param string $scenario
     * @param string $listName
     * @return array
     * @throws \Exception
     */
    private function getScenarioFields($scenario, $listName)
    {
        if ($scenario === null) {
            $scenario = $this->getScenario();
        }
        if ($scenario === null) {

            return static::${$listName};
        }
        if (!isset(static::${$listName}[$scenario])) {

            throw new \Exception("Scenario $scenario not found in " . __CLASS__ . " $listName");
        }

        return static::${$listName}[$scenario];
    }
}