<?php

namespace App\Models\Base;

use Illuminate\Database\Eloquent\Model as EloquentModel;

/**
 * Base Model parent class
 *
 * Class BaseNode
 * @package App\Models\Base\
 */
abstract class Model extends EloquentModel
{
    use ExtendedModel;

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'createdAt';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'updatedAt';

    /**
     * The name of the "deleted at" column.
     *
     * @var string
     */
    const DELETED_AT = 'deletedAt';

    /**
     * Common create and update scenario
     *
     * @const string
     */
    const SCENARIO_SAVE = 'save';
}