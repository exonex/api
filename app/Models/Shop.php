<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

use App\Models\Base\Model;

/**
 * Магазин
 *
 * Class Shop
 * @property integer $id                ID
 * @property string  $countryId         ID страны
 * @property integer $stateId           ID региона
 * @property string  $name              Название
 * @property array   $address           Адрес (JSON)
 * @property array   $description       Описание (JSON)
 * @property array   $contacts          Контакты (JSON)
 * @property Carbon  $createdAt
 * @property Carbon  $updatedAt
 * @property Carbon  $deletedAt
 *
 * @property-read BelongsTo|Country             $country             Страна
 * @property-read BelongsTo|State               $state               Регион
 * @property-read HasMany|Product[]             $products            Товары в магазине
 */
class Shop extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model
     *
     * @var string
     */
    protected $table = 'shops';

    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'countryId',
        'stateId',
        'name',
        'description',
        'address',
        'contacts',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'address'     => 'array',
        'description' => 'array',
        'contacts'    => 'array',
    ];

    /**
     * Страна
     *
     * @return BelongsTo|Country
     */
    public function country()
    {
        return $this->belongsTo(Country::class, 'countryId', 'id');
    }

    /**
     * Регион
     *
     * @return BelongsTo|State
     */
    public function state()
    {
        return $this->belongsTo(State::class, 'stateId', 'id');
    }

    /**
     * Товары в магазине
     *
     * @return HasMany|Product[]
     */
    public function products()
    {
        return $this->hasMany(Product::class, 'shopId', 'id');
    }
}
