<?php

namespace App\Models;

use Carbon\Carbon;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

use App\Models\Base\AuthUser;

/**
 * Модель пользователя
 * App\Models\User
 *
 * @property integer $id
 * @property string  $name          Имя (и отчество)
 * @property string  $lastName      Фамилия
 * @property string  $nickname      Ник
 * @property string  $gender        Пол '' - не указан, 'm' - м, 'f' - ж
 * @property string  $birthdate     Дата рождения
 * @property string  $avatar        Аватарка
 * @property string  $phone         Телефон
 * @property string  $email         Почта
 * @property string  $password      Хэш пароля
 *
 * @property integer $countryId     Страна
 * @property integer $stateId       Регион
 * @property string  $city          Город
 * @property array   $settings      Дополнительные параметры пользователя в JSON
 *
 * @property string  $rememberToken Токен авторизации
 * @property Carbon  $createdAt
 * @property Carbon  $updatedAt
 * @property Carbon  $deletedAt
 *
 * @property-read BelongsTo|Country      $country Страна
 * @property-read BelongsTo|State        $state   Регион
 * @property-read BelongsToMany|UserRole $roles   Роли пользователя
 */
class User extends AuthUser
{
    use SoftDeletes,    // Мягкое удаление
        HasApiTokens,   // Laravel Passport
        Notifiable;     // Уведомления

    /**
     * Common create and update scenario
     *
     * @const string
     */
    const SCENARIO_REGISTER = 'register';

    /**
     * Email update scenario
     *
     * @const string
     */
    const SCENARIO_EMAIL = 'email';

    /**
     * Nickname update scenario
     *
     * @const string
     */
    const SCENARIO_NICKNAME = 'nickname';

    /**
     * Users's genders
     *
     * @const
     */
    const GENDER_UNDEFINED = '';
    const GENDER_MALE = 'm';
    const GENDER_FEMALE = 'f';

    /**
     * The table associated with the model
     * 
     * @var string
     */
    protected $table = 'users';

    /**
     * The column name of the "remember me" token.
     *
     * @var string
     */
    protected $rememberTokenName = 'rememberToken';

    /**
     * The attributes that are mass assignable
     * 
     * @var array
     */
    protected $fillable = [
        'name',
        'lastName',
        'nickname',
        'email',
        'gender',
        'birthdate',
        'avatar',
        'phone',
        'countryId',
        'stateId',
        'city',
        'password',
        'settings',
    ];

    /**
     * The attributes that should be hidden for arrays
     * 
     * @var array
     */
    protected $hidden = [
        'password',
        'rememberToken',
    ];
    
    /**
     * The attributes that should be mutated to dates
     *
     * @var array
     */
    protected $dates = [
        self::DELETED_AT,
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'settings' => 'array',
    ];

    /**
     * Правила валидации
     *
     * @var array
     */
    protected static $rules = [
        self::SCENARIO_REGISTER => [
            'name'     => 'required|string|max:255',
            'lastName' => 'required|string|max:255',
            'email'    => 'required|email',
            'password' => 'required|string|min:6|max:32',
            'nickname' => 'string|min:0|max:32',
            'passwordConfirmation' => 'required|same:password',
        ],
        self::SCENARIO_SAVE => [
            'name'      => 'required|string|max:255',
            'lastName'  => 'required|string|max:255',
            'email'     => 'required|email|min:0|max:255',
            'password'  => 'required|min:6|max:32',
            'avatar'    => 'string|max:255|nullable',
            'nickname'  => 'string|min:0|max:32|nullable',
            'phone'     => 'min:6|max:20|nullable',
            'birthdate' => 'date|nullable',
            'gender'    => 'string|max:1|in:' . self::GENDER_UNDEFINED . ',' . self::GENDER_FEMALE . ',' . self::GENDER_MALE,
            'countryId' => 'integer|min:1|nullable',
            'stateId'   => 'integer|min:1|nullable',
            'city'      => 'string|max:255',
            'settings'  => 'array|nullable',
        ],
        self::SCENARIO_EMAIL => [
            'email' => 'required|email|min:0|max:255',
        ],
        self::SCENARIO_NICKNAME => [
            'nickname'  => 'string|min:0|max:32|nullable',
        ],
    ];

    /**
     * Unique user fields
     *
     * @var array
     */
    protected static $unique = [
        self::SCENARIO_REGISTER => ['email', 'nickname'],
        self::SCENARIO_SAVE     => ['email', 'nickname'],
        self::SCENARIO_EMAIL    => ['email'],
        self::SCENARIO_NICKNAME => ['nickname'],
    ];

    /**
     * Validation fail messages
     *
     * @var array
     */
    protected static $messages = [
        'name.required'        => 'Необходимо указать имя',
        'lastName.required'    => 'Необходимо указать фамилию',
        'email.required'       => 'Необходимо указать Email',
        'email.email'          => 'Неверный формат Email',
        'email.unique'         => 'Данный Email уже используется в системе',
        'password.required'    => 'Необходимо указать пароль',
        'password.min'         => 'Минимальная длина пароля - 6 символов',
        'password.max'         => 'Максимальная длина пароля - 32 символов',
        'nickname.unique'      => 'Никнейм уже занят другим пользователем',
        'birthdate.date'       => 'Неверный формат даты рождения',
        'phone.required'       => 'Необходимо указать телефон',
        'phone.max'            => 'Максимальная длина телефона - 20 символа',
        'phone.min'            => 'Минимальная длина телефона - 6 символов',
        'gender.integer'       => 'Поле gender должно быть целочисленным',
        'gender.in'            => 'Поле gender должно быть в интервале  [1, 2, 3]',
        'passwordConfirmation.required' => 'Необходимо подтвердить пароль',
        'passwordConfirmation.same'     => 'Пароль не совпадает с подтверждением',
    ];

    /**
     * Validation & save scenario
     *
     * @var string
     */
    protected $scenario = self::SCENARIO_SAVE;

    /**
     * Валидация email
     *
     * @param string $email
     * @param mixed $exceptId
     */
    public function passOrFailEmail(string $email, $exceptId = null)
    {
        $this->passesOrFailField('email', $email, $exceptId);
    }

    /**
     * Валидация ника
     *
     * @param string $nickname
     * @param mixed $exceptId
     */
    public function passOrFailNickname(string $nickname, $exceptId = null)
    {
        $this->passesOrFailField('nickname', $nickname, $exceptId);
    }

    /**
     * Страна
     *
     * @return BelongsTo|Country
     */
    public function country()
    {
        return $this->belongsTo(Country::class, 'countryId', 'id');
    }

    /**
     * Регион
     *
     * @return BelongsTo|State
     */
    public function state()
    {
        return $this->belongsTo(State::class, 'stateId', 'id');
    }

    /**
     * Список ролей
     *
     * @return BelongsToMany|UserRole[]
     * @todo Реализовать softDeletes для связки с ролями
     */
    public function roles()
    {
        return $this->belongsToMany(UserRole::class, UserRole::USERS_PIVOT_TABLE, 'userId', 'roleId')
            ->with(UserRole::USERS_PIVOT_TABLE . '.' . static::DELETED_AT, null)
            ->withTimestamps(static::CREATED_AT, static::UPDATED_AT);
    }

    /**
     * Проверка роли
     *
     * @param int|UserRole $role Объект роли или ID
     * @return bool
     */
    public function hasRole($role)
    {
        $result = $this->roles();
        if ($role instanceof UserRole) {
            $result = $result->where('id', '=', $role->id);
        } elseif (is_string($role)) {
            $result = $result->where('id', '=', $role);
        }

        return $result->count() > 0;
    }

    /**
     * Флаг администраторских прав
     *
     * @return bool
     */
    public function isAdmin()
    {
        return $this->hasRole(UserRole::ADMIN);
    }

    /**
     * Назначение роли
     *
     * @param string|UserRole $role Объект роли или ID
     * @return void
     */
    public function assignRole($role)
    {
        $this->roles()->attach($role);
    }

    /**
     * Удаление роли
     *
     * @param string|UserRole $role Объект роли или ID
     * @return int
     */
    public function removeRole($role)
    {
        return $this->roles()->detach($role);
    }

    /**
     * Получение полного имени
     *
     * @return string
     */
    public function getFullName()
    {
        return $this->name . ($this->lastName ? ' ' . $this->lastName : '');
    }

    /**
     * Получение пользователя по адресу электронной почты
     * В случае неудачи и флага $orFail бросает исключение ModelNotFoundException.
     *
     * @param string $email Почта пользоватетеля
     * @param bool $orFail  Флаг, обозначающий, вызывать ли исключение, если пользователь не найден
     * @return User|null
     * @throws ModelNotFoundException
     */
    public static function whereEmail(string $email, bool $orFail = true)
    {
        $result = self::where('email', '=', $email);

        return $orFail ? $result->firstOrFail() : $result->first();
    }

    /**
     * Получение подписи пользователя на основе IP и токена авторизации
     *
     * @return string
     */
    public function getFingerprint()
    {
        // Используем только первую половину адреса, т.к. вторая иногда меняется по желанию провайдера
        $ipAddress = explode('.', $_SERVER['REMOTE_ADDR']);

        return sha1($this->id . $this->email . 'Boris has arrived! (RA2YR)' . $ipAddress[0] . 'Yippee-ki-yay!' . $ipAddress[1]);
    }
}
