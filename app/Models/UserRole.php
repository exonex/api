<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

use App\Models\Base\Model;

/**
 * Справочник ролей пользователя
 * App\Models\UserRole
 *
 * @property string  $id
 * @property string  $name              Название
 * @property string  $description       Описание
 * @property integer $order             Порядок сортировки
 * @property Carbon  $createdAt
 * @property Carbon  $updatedAt
 * @property Carbon  $deletedAt
 *
 * @property-read User[] $users         Пользователи с указанной ролью
 */
class UserRole extends Model
{
    use SoftDeletes;

    /** @const ID группы администраторов */
    const ADMIN = 'adm';
    /** @const ID группы менеджеров */
    const MANAGER = 'mgr';
    /** @const ID группы модераторов */
    const MODERATOR = 'mdr';
    /** @const ID группы клиентов */
    const CUSTOMER = 'cst';
    /** @const ID группы зарегистрированных пользователей */
    const USER = 'usr';

    /** @const ID забанненых пользователей */
    const BANNED = 'ban';

    /** @const string Таблица связки с пользователями */
    const USERS_PIVOT_TABLE = 'usersRoles';

    /**
     * The table associated with the model
     * @var string
     */
    protected $table = 'userRoles';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
    ];

    /**
     * The attributes that should be mutated to dates
     *
     * @var array
     */
    protected $dates = [
        self::DELETED_AT,
    ];

    /**
     * Список пользователей
     * 
     * @return BelongsToMany|User[]
     * @todo Реализовать softDeletes и для связки тоже
     */
    public function users()
    {
        return $this->belongsToMany(User::class, static::USERS_PIVOT_TABLE, 'roleId', 'userId')
            ->with(static::USERS_PIVOT_TABLE . '.' . static::DELETED_AT, null)
            ->withTimestamps(static::CREATED_AT, static::UPDATED_AT);
    }
}
