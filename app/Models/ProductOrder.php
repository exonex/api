<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

use App\Models\Base\Model;

/**
 * Заказ
 *
 * Class ProductOrder
 * @property integer $id            ID
 * @property integer $userId        ID пользователя
 * @property integer $shopId        ID магазина
 * @property integer $cost          Общая стоимость товара
 * @property string  $description   Описание заказа
 * @property Carbon  $createdAt
 * @property Carbon  $updatedAt
 * @property Carbon  $deletedAt
 *
 * @property-read BelongsTo|User            $user       Пользователь
 * @property-read BelongsTo|Shop            $shop       Магазин
 * @property-read BelongsToMany|Product[]   $products   Товары заказа
 */
class ProductOrder extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model
     *
     * @var string
     */
    protected $table = 'productOrders';

    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'userId',
        'shopId',
        'cost',
        'description',
    ];

    /**
     * Связь с моделью пользователя
     *
     * @return BelongsTo|User
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'userId', 'id');
    }

    /**
     * Магазин, в котором доступен товар
     *
     * @return BelongsTo|Shop
     */
    public function shop()
    {
        return $this->belongsTo(Shop::class, 'shopId', 'id');
    }

    /**
     * Список товаров
     *
     * @return BelongsToMany|Product[]
     */
    public function products()
    {
        return $this->belongsToMany(Product::class, 'productsInOrders', 'orderId', 'productId')
            ->withPivot(['count', static::CREATED_AT, static::UPDATED_AT]);
    }
}
