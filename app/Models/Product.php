<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

use App\Models\Base\Model;

/**
 * Товар в магазине
 *
 * Class Product
 * @property integer $id            ID
 * @property integer $modelId       ID модели
 * @property integer $shopId        ID магазина
 * @property integer $count         Доступное количество
 * @property integer $status        Статус товара
 * @property Carbon  $createdAt
 * @property Carbon  $updatedAt
 *
 * @property-read BelongsTo|ProductModel            $model      Модель
 * @property-read BelongsTo|Shop                    $shop       Магазин
 * @property-read BelongsToMany|ProductOrder[]      $orders     Список заказов, в которых есть данный товар
 *
 * @package App
 */
class Product extends Model
{
    /**
     * The table associated with the model
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'modelId',
        'shopId',
        'count',
        'status',
    ];

    /**
     * Модель товара
     *
     * @return BelongsTo|ProductModel
     */
    public function model()
    {
        return $this->belongsTo(ProductModel::class, 'modelId', 'id');
    }

    /**
     * Магазин, в котором доступен товар
     *
     * @return BelongsTo|Shop
     */
    public function shop()
    {
        return $this->belongsTo(Shop::class, 'shopId', 'id');
    }

    /**
     * Список заказов, в которых есть данный товар
     *
     * @return BelongsToMany|ProductOrder[]
     */
    public function orders()
    {
        return $this->belongsToMany(ProductOrder::class, 'productsInOrders', 'productId', 'orderId')
            ->withPivot(['count', static::CREATED_AT, static::UPDATED_AT]);
    }
}
