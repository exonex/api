<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

use App\Models\Base\Node;
use App\Traits\ChildrenCount;

/**
 * Категория товаров
 *
 * Class ProductCategory
 * @property integer $id            ID
 * @property string  $name          Название
 * @property string  $pathName      Список родительских категорий, разделённый /
 * @property string  $path          Список ID родителей, разделённый /
 * @property string  $description   Описание
 * @property array   $measures      Характеристики модели
 * @property integer $parentId      ID родителя
 * @property integer $nsLeft        Поля nested set
 * @property integer $nsRight       Поля nested set
 * @property integer $nsDepth       Поля nested set
 * @property integer $order         Порядок сортировки
 * @property Carbon  $createdAt
 * @property Carbon  $updatedAt
 * @property Carbon  $deletedAt
 *
 * @property-read BelongsTo|ProductCategory|null     $parent    Родительская категория
 * @property-read HasMany|ProductModel[]             $models    Модели товаров в категории
 *
 */
class ProductCategory extends Node
{
    use SoftDeletes, ChildrenCount;

    /**
     * The table associated with the model
     *
     * @var string
     */
    protected $table = 'productCategories';

    /**
     * Column to perform the default sorting
     *
     * @var string
     */
    protected $orderColumn = 'order';

    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'parentId',
        'name',
        'pathName',
        'path',
        'description',
        'measures',
        'order',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'measures' => 'array',
    ];

    /**
     * validation rules
     *
     * @var array
     */
    protected static $rules = [
        // Save instance
        self::SCENARIO_SAVE => [
            'name'        => 'required|string|max:255',
            'pathName'    => 'string|max:255',
            'path'        => 'string|max:255',
            'description' => 'string|nullable|max:255',
            'measures'    => 'string',
            'parentId'    => 'integer|nullable|exists:productCategories,id',
            'order'       => 'integer|nullable',
        ],
    ];

    /**
     * Unique fields
     *
     * @var array
     */
    protected static $unique = [
        self::SCENARIO_SAVE => []
    ];

    /**
     * Validation fail messages
     *
     * @var array
     */
    protected static $messages = [];

    /**
     * Модели товаров в категории
     *
     * @return HasMany|ProductModel[]
     */
    public function models()
    {
        return $this->hasMany(ProductModel::class, 'modelId', 'id');
    }
}
