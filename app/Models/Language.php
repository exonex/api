<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\Base\Model;

/**
 * Справочник языков
 * App\Models\Language
 *
 * @property string  $id            Строковый ID Alpha2 (ISO 639-1)
 * @property string  $name          Название
 * @property string  $nameEn        Название на английском языке
 * @property string  $iso2          Код ISO 639-2
 * @property string  $iso3          Код ISO 639-3
 * @property string  $gost          Код ГОСТ 7.75–97
 * @property string  $code          Числовой код
 * @property integer $order         Порядок сортировки
 */
class Language extends Model
{
    use SoftDeletes;

    /** Алиасы языков */
    const ENGLISH    = 'en';
    const BELARUSIAN = 'be';
    const KAZAKH     = 'kk';
    const RUSSIAN    = 'ru';
    const UKRAINIAN  = 'uk';

    /**
     * The table associated with the model
     *
     * @var string
     */
    protected $table = 'languages';

    /**
     * Indicates if the model should be timestamped
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
    ];

    /**
     * validation rules
     *
     * @var array
     */
    protected static $rules = [
        // Save instance
        self::SCENARIO_SAVE => [
            'id'      => 'required|string|size:2',
            'name'    => 'required|string|max:255',
            'nameEn'  => 'required|string|min:2|max:255',
            'iso2'    => 'string|size:3',
            'iso3'    => 'string|size:3',
            'gost'    => 'string|size:3',
            'code'    => 'string|size:3',
            'order'   => 'integer|nullable',
        ],
    ];

    /**
     * Unique fields
     *
     * @var array
     */
    protected static $unique = [
        self::SCENARIO_SAVE => ['id', 'id3', 'name', 'nameEn', 'iso2', 'iso3', 'gost'],
    ];

    /**
     * Validation fail messages
     *
     * @var array
     */
    protected static $messages = [];

    /**
     * Получение url флага
     *
     * @param int $size Размер флага в пикселях. Пока поддерживается только 16, 24, 32 и 48
     * @return string
     */
    public function getFlagUrl(int $size = 16)
    {
        return "images/languages/{$size}/{$this->id}.png";
    }
}
