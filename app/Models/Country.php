<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\HasMany;

use App\Models\Base\Model;

/**
 * Справочник стран
 * App\Models\Country
 *
 * @property string  $id            Строковый ID страны (2 символа - RU, US,...)
 * @property string  $id3           Строковый ID страны (3 символа - RUS, USA,...)
 * @property string  $name          Общепринятое название
 * @property string  $fullName      Полное название
 * @property string  $nameEn        Общепринятое название на английском языке
 * @property string  $code          Международный числовой код (3 символа)
 * @property string  $phoneCode     Телефонный код
 * @property string  $domain        Доменная зона страны
 * @property integer $order         Порядок сортировки
 *
 * @property-read HasMany|State[]           $states        Регионы страны
 * @property-read HasMany|User[]            $users         Пользователи страны
 * @property-read HasMany|Shop[]            $shops         Магазины страны
 * @property-read HasMany|ProductCompany[]  $companies     Производители страны
 */
class Country extends Model
{
    use SoftDeletes;

    /** Countries aliases (and PK too) */
    const BELARUS     = 'BY';
    const KAZAKHSTAN  = 'KZ';
    const RUSSIA      = 'RU';
    const UKRAINE     = 'UA';
    const USA         = 'US';

    /**
     * The table associated with the model
     *
     * @var string
     */
    protected $table = 'countries';

    /**
     * Indicates if the model should be timestamped
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
    ];

    /**
     * validation rules
     *
     * @var array
     */
    protected static $rules = [
        self::SCENARIO_SAVE => [
            'id'         => 'required|string|size:2',
            'id3'        => 'required|string|size:3',
            'name'       => 'required|string|max:255',
            'fullName'   => 'required|string|min:2|max:255',
            'nameEn'     => 'required|string|min:2|max:255',
            'code'       => 'string|max:3',
            'phoneCode'  => 'string|max:8',
            'domain'     => 'string|size:2',
            'order'      => 'integer|nullable',
        ],
    ];

    /**
     * Unique fields
     *
     * @var array
     */
    protected static $unique = [
        self::SCENARIO_SAVE => ['id', 'id3', 'name', 'fullName', 'nameEn', 'domain'],
    ];

    /**
     * Validation fail messages
     *
     * @var array
     */
    protected static $messages = [];

    /**
     * Regions list
     *
     * @return HasMany|State[]
     */
    public function states()
    {
        return $this->hasMany(State::class, 'countryId', 'id');
    }

    /**
     * Users list
     *
     * @return HasMany|User[]
     */
    public function users()
    {
        return $this->hasMany(User::class, 'countryId', 'id');
    }

    /**
     * Flag's url getter
     *
     * @param int $size Flag's size in pixels. Now 16, 24, 32, 48
     * @return string
     */
    public function getFlagUrl(int $size = 16)
    {
        return "images/flags/{$size}/{$this->id}.png";
    }

    /**
     * Companies of the country
     *
     * @return HasMany|ProductCompany[]
     */
    public function companies()
    {
        return $this->hasMany(ProductCompany::class, 'companyId', 'id');
    }

    /**
     * Shops in the country
     *
     * @return HasMany|Shop[]
     */
    public function shops()
    {
        return $this->hasMany(Shop::class, 'countryId', 'id');
    }
}
