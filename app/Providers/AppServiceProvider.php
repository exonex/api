<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Providers\Services\UserServiceProvider;
use App\Providers\Handlers\CallHandlerProvider;
use App\Providers\Handlers\ErrorHandlerProvider;
use App\Providers\Services\Crud\CountryServiceProvider;
use App\Providers\Handlers\LogDispatcherProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Common services
        $this->app->register(UserServiceProvider::class);

        // Crud services
        $this->app->register(CountryServiceProvider::class);

        // Handlers
        $this->app->register(CallHandlerProvider::class);
        $this->app->register(ErrorHandlerProvider::class);
        $this->app->register(LogDispatcherProvider::class);
    }
}
