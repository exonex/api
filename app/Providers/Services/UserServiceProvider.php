<?php

namespace App\Providers\Services;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

use App\Services\UserService;

class UserServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(UserService::class, function ($app) {
            return new UserService(Auth::user());
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [UserService::class];
    }

}
