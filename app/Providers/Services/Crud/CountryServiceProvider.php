<?php

namespace App\Providers\Services\Crud;

use Illuminate\Support\ServiceProvider;

use App\Services\Crud\CountryService;

class CountryServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(CountryService::class, function ($app) {
            return new CountryService();
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [CountryService::class];
    }

}
