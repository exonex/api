<?php

namespace App\Providers\Handlers;

use Illuminate\Support\ServiceProvider;

use App\Contracts\CallHandler;
use App\Handlers\DbLogCallHandler;

/**
 * Class CallHandlerProvider
 *
 * @package App\Providers\Handlers
 */
class CallHandlerProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(CallHandler::class, function ($app) {
            return app(DbLogCallHandler::class); // Use DbLogCallHandler for all CallHandler interface uses
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [CallHandler::class];
    }

}
