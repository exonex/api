<?php

namespace App\Providers\Handlers;

use Illuminate\Support\ServiceProvider;

use App\Contracts\ErrorHandler;
use App\Handlers\DbLogErrorHandler;

/**
 * Class ErrorHandlerProvider
 *
 * @package App\Providers\Handlers
 */
class ErrorHandlerProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(ErrorHandler::class, function ($app) {
            return app(DbLogErrorHandler::class); // Use DbLogErrorHandler for all ErrorHandler uses
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [ErrorHandler::class];
    }

}
