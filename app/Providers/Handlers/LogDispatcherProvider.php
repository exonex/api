<?php

namespace App\Providers\Handlers;

use Illuminate\Support\ServiceProvider;

use App\Contracts\ErrorHandler;
use App\Contracts\LogDispatcher;
use App\Handlers\LogApiCallDispatcher;

/**
 * Class LogApiCallDispatcherProvider
 *
 * @package App\Providers\Handlers
 */
class LogDispatcherProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(LogDispatcher::class, function ($app) {
            return app(LogApiCallDispatcher::class); // Use LogApiCallDispatcher for all LogDispatcher uses
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [LogDispatcher::class];
    }

}
