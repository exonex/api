<?php

namespace App\Events;

use Exception;
use Illuminate\Queue\SerializesModels;

class UnexpectedExceptionRaised extends Event
{
    use SerializesModels;

    /**
     * @public Exception
     */
    public $exception;

    /**
     * @public string
     */
    public $addMessage;

    /**
     * @public array
     */
    public $params;

    /**
     * UnexpectedExceptionRised constructor
     *
     * @param Exception $exception
     * @param string $addMessage
     * @param array $params
     */
    public function __construct(Exception $exception, string $addMessage = '', array $params = [])
    {
        $this->exception = $exception;
        $this->addMessage = $addMessage;
        $this->params = $params;
    }
}