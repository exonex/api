<?php

namespace App\Events\User;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;

use App\Models\User;
use App\Models\UserToken;

/**
 * Событие регистрации пользователя
 */
class UserWasRegistered extends Event
{
    use SerializesModels;

    /** @const Пользователь зарегистрировался через сайт */
    const AT_SITE = 'site';
    /** @const Пользователь зарегистрировался через соцсеть */
    const WITH_SOCIAL = 'social';

    /** @var User Объект зарегистрированного пользователя */
    public $user;

    /**
     * Модель связи пользователя с соцсетью, если он зарегистрировался через неё
     * или модель токена активации пользователя, если он зарегистрировался через сайт
     *
     * @var UserToken
     */
    public $extendData;

    /** @var string Источник регистрации - 'site' или 'social' */
    public $registeredThrough;

    /**
     * Create a new event instance.
     *
     * @param User $user Модель зарегистрировавшегося пользователя
     * @param string $registeredThrough Источник регистрации - 'site' или 'social'
     * @param UserToken $extendData Дополнительные данные для события. Связь с соцсетью, или токен активации
     */
    public function __construct(User $user, string $registeredThrough, $extendData = null)
    {
        $this->user = $user;
        $this->registeredThrough = $registeredThrough;
        $this->extendData = $extendData;
    }
}
