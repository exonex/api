<?php

namespace App\Events\User;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;

use App\Models\User;
use App\Models\UserToken;

/**
 * Событие запроса пользователем сброса пароля
 */
class UserAskedPasswordReset extends Event
{
    use SerializesModels;

    /** @var User Объект зарегистрированного пользователя */
    public $user;

    /** @var UserToken Модель токена активации пользователя, если он зарегистрировался через сайт */
    public $token;

    /**
     * Create a new event instance.
     *
     * @param User $user Модель зарегистрировавшегося пользователя
     * @param UserToken $userToken Токен сброса пароля
     */
    public function __construct(User $user, UserToken $userToken)
    {
        $this->user = $user;
        $this->token = $userToken;
    }
}
