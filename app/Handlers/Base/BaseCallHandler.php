<?php

namespace App\Handlers\Base;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;

use App\Contracts\CallHandler;

/**
 * Base Call Handler
 *
 * Class BaseCallHandler
 * @package App\Handlers\Base
 */
abstract class BaseCallHandler implements CallHandler
{
    /**
     * Request handling start microtime
     *
     * @var float
     */
    protected $startedAt = 0;

    /**
     * Request handling finish microtime
     *
     * @var float
     */
    protected $finishedAt = 0;

    /**
     * Error logs ids
     *
     * @var array
     */
    protected $errors = [];

    /**
     * Request log ID
     *
     * @var mixed
     */
    protected $request;

    /**
     * Response log ID
     *
     * @var mixed
     */
    protected $response;

    /**
     * Execution runtime in ms
     *
     * @param bool $stopTimer
     * @return float|null
     */
    public function getRuntime(bool $stopTimer = false)
    {
        if (!$this->startedAt) {

            return null;
        }
        if ($stopTimer) {
            $this->stopTimer();
        }

        return round(($this->finishedAt ?: microtime(true)) - $this->startedAt, 3);
    }

    /**
     * Finished time update
     */
    protected function stopTimer(): void
    {
        $this->finishedAt = microtime(true);
    }

    /**
     * Inner timer start
     */
    protected function startTimer(): void
    {
        $this->startedAt = microtime(true);
    }

    /**
     * Add new error
     *
     * @param mixed $error
     */
    public function addError($error): void
    {
        $this->errors[] = $error;
    }

    /**
     * Errors list getter
     *
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * Request getter
     *
     * @return mixed
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * Request ID(code, hash,...) getter
     *
     * @return int|string
     */
    abstract public function getRequestId();

    /**
     * Request setter
     *
     * @param mixed $request
     */
    public function setRequest($request): void
    {
        $this->request = $request;
    }
    /**
     * Response getter
     *
     * @return int
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * Response setter
     *
     * @param mixed $response
     */
    public function setResponse($response): void
    {
        $this->response = $response;
    }

    /**
     * Request logging
     *
     * @param Request $request
     */
    abstract public function handleRequest(Request $request): void;

    /**
     * Response logging
     *
     * @param JsonResponse|Response $response
     */
    abstract public function handleResponse($response): void;

    /**
     * Current call debug info
     *
     * @return array
     */
    abstract public function getCallInfo(): array;
}