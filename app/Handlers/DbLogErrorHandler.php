<?php

namespace App\Handlers;

use App\Models\LogError;
use App\Contracts\CallHandler;
use App\Contracts\ErrorHandler;

/**
 * Error DB-logging handler
 *
 * Class DbLogErrorHandler
 * @package App\Handlers
 */
class DbLogErrorHandler implements ErrorHandler
{
    /**
     * Used call handler for getting request info
     *
     * @var CallHandler
     */
    protected $callHandler;

    /**
     * Last error log ID
     *
     * @var int
     */
    protected $logId;

    /**
     * ErrorDbHandler constructor.
     *
     * @param CallHandler $callHandler
     */
    public function __construct(CallHandler $callHandler)
    {
        $this->callHandler = $callHandler;
    }

    /**
     * Last error log ID getter
     *
     * @return int
     */
    public function getLogId()
    {
        return $this->logId;
    }

    /**
     * @param array $record Array like that:
     * [
     *  'message'    => 'Ho-ho-ho exception has been raised',
     *  'context'    => [
     *      'userId'    => 1,
     *      'email'     => 'admin@admin.com',
     *      'exception' => Exception,
     *  ],
     *  'level'      => 400,
     *  'level_name' => 'ERROR',
     *  'channel'    => 'local'
     *  'datetime'   => DateTime,
     *  'extra'      => [],
     *  'formatted'  => '[2018-12-16 10:10:11] local.ERROR: Ho-ho-ho exception has been raised {"userId":1,"email":"admin@admin.com","exception":"[object] (Exception(code: 0): Ho-ho-ho exception has been raised at /var/www/app/Http/Controllers/API/BaseController.php:24) [stacktrace] ...'
     * ]
     *
     * @see AbstractProcessingHandler::write()
     */
    public function handleError(array $record): void
    {
        try {
            $data = [
                'type' => $record['level_name'],
                'message' => $record['message'],
                'data' => $record['formatted'],
            ];
            if ($this->callHandler) {
                $data['requestId'] = $this->callHandler->getRequest();
            }
            if (isset($record['context']['exception'])) {
                /** @var \Exception $exception */
                $exception = $record['context']['exception'];
                $data['file'] = $exception->getFile();
                $data['line'] = $exception->getLine();
                $data['trace'] = $exception->getTraceAsString();
            }
            $logEntry = new LogError($data);
            $logEntry->save();
            $this->logId = $logEntry->id;
            $this->callHandler->addError($logEntry->id);

        // We shouldn't handle self errors with this handler to avoid loop
        } catch (\Throwable$e) {
            // @todo Emergency error handling
        }
    }
}