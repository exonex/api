<?php

namespace App\Handlers;

use App\Contracts\LogDispatcher;
use App\Models\LogApiCall;
use App\Models\LogApiCallRule;

/**
 * DB-logging dispatcher
 *
 * Class LogApiCallDispatcher
 * @package App\Handlers
 */
class LogApiCallDispatcher implements LogDispatcher
{
    /**
     * Comparable fields
     *
     * @const string[]
     */
    const COMPARABLE_FIELDS = ['userId', 'type', 'code', 'action', 'tag', 'ip'];

    /**
     * All rules list
     *
     * @var LogApiCallRule[]|null
     */
    protected static $rules;

    /**
     * All rules getter
     *
     * @return LogApiCallRule[]
     */
    public function getRules(): array
    {
        if (static::$rules === null) {
            static::$rules = LogApiCallRule::all()->all();
        }

        return static::$rules;
    }

    /**
     * Active rules getter for current log entry
     *
     * @param LogApiCall $logEntry
     * @return LogApiCallRule[]
     */
    public function getActiveRules($logEntry): array
    {
        $activeRules = [];
        foreach ($this->getRules() as $rule) {
            foreach (static::COMPARABLE_FIELDS as $field) {
                if ($rule->$field != LogApiCallRule::ANY && $logEntry->$field != $rule->$field) {
                    continue 2;
                }
            }
            $activeRules[] = $rule;
        }
        return $activeRules;
    }

    /**
     * Check if we need save current log entry
     *
     * @param LogApiCall $logEntry
     * @return bool
     */
    public function needSave($logEntry): bool
    {
        // Get all rules available for current log entry
        // We need at least one base rule to allow log all
        if (!$activeRules = $this->getActiveRules($logEntry)) {

            return false;
        }
        // We need at least one deny rule or all allowing
        foreach ($activeRules as $activeRule) {
            if (!$activeRule->save) {

                return false;
            }
        }

        return true;
    }
}