<?php

namespace App\Handlers;

use App\Contracts\LogDispatcher;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

use App\Models\LogApiCall;
use App\Handlers\Base\BaseCallHandler;

/**
 * Class ApiCallHandler
 * Custom API calls handler for DB logging. Use LogApiCall model
 *
 * @package App\Handlers
 */
class DbLogCallHandler extends BaseCallHandler
{
    /**
     * @var LogDispatcher
     */
    protected $dispatcher;

    /**
     * DbLogCallHandler constructor.
     *
     * @param LogDispatcher $dispatcher
     */
    public function __construct(LogDispatcher $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    /**
     * Request logging
     *
     * @param Request $request
     */
    public function handleRequest(Request $request): void
    {
        // Start timer
        $this->startTimer();

        // Simple logging
        Log::info($request->getUri());

        // Create and save request log
        $logEntry = LogApiCall::createRequestLog([
            'code' => $request->method() . ($request->isXmlHttpRequest() ? '|ajax' : ''),
            'data' => $request->getUri(),
        ]);

        // Check if we need save log entry
        if ($this->dispatcher->needSave($logEntry)) {
            $logEntry->save();
            // Save request log ID for response logging
            $this->setRequest($logEntry->id);
        }
    }

    /**
     * Response logging
     *
     * @param JsonResponse|Response $response
     */
    public function handleResponse($response): void
    {
        Log::info('response', [$response->getContent()]);

        // Create and save response log
        $logEntry = LogApiCall::createResponseLog([
            'requestId' => $this->getRequest(),
            'code'      => $response->status(),
            'data'      => $response->getContent(),
            'runtime'   => $this->getRuntime(),
        ]);

        // Check if we need save log entry
        if ($this->dispatcher->needSave($logEntry)) {
            $logEntry->save();
            // Save response log ID
            $this->setResponse($logEntry->id);
        }
    }

    /**
     * Current call debug info
     *
     * @return array
     */
    public function getCallInfo(): array
    {
        $info = [
            'userId'     => $this->getUserId(),
            'runtime'    => $this->getRuntime(),
            'requestId'  => $this->getRequest(),
            'responseId' => $this->getResponse(),
        ];
        if ($errorIds = $this->getErrors()) {
            $info['errorId'] = count($errorIds) == 1
                ? reset($errorIds)
                : implode(',', $errorIds);
        }

        return $info;
    }

    /**
     * Authenticated user id
     *
     * @return int|null
     */
    public function getUserId()
    {
        return !Auth::guest() ? Auth::user()->id : null;
    }

    /**
     * Request ID getter
     *
     * @return int
     */
    public function getRequestId()
    {
        return $this->request; // This class store request ID only
    }
}