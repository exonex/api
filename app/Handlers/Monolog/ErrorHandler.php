<?php

namespace App\Handlers\Monolog;

use Monolog\Logger;
use Monolog\Handler\AbstractProcessingHandler;

use \App\Contracts\ErrorHandler as ErrorHandlerContract;

/**
 * Class ErrorDbHandler
 * Monolog-compatible exception handler for DB logging. Use LogError model
 *
 * @package App\Handlers
 */
class ErrorHandler extends AbstractProcessingHandler
{
    /**
     * Custom error handler
     *
     * @var ErrorHandlerContract
     */
    protected $errorHandler;

    /**
     * ErrorLogHandler constructor
     *
     * @param int $level
     * @param bool $bubble
     * @param ErrorHandlerContract $errorHandler
     */
    public function __construct($level = Logger::ERROR, $bubble = true, ErrorHandlerContract $errorHandler)
    {
        parent::__construct($level, $bubble);
        $this->errorHandler = $errorHandler;
    }

    /**
     * @param array $record Array like that:
     * [
     *  'message'    => 'Ho-ho-ho exception has been raised',
     *  'context'    => [
     *      'userId'    => 1,
     *      'email'     => 'admin@admin.com',
     *      'exception' => Exception,
     *  ],
     *  'level'      => 400,
     *  'level_name' => 'ERROR',
     *  'channel'    => 'local'
     *  'datetime'   => DateTime,
     *  'extra'      => [],
     *  'formatted'  => '[2018-12-16 10:10:11] local.ERROR: Ho-ho-ho exception has been raised {"userId":1,"email":"admin@admin.com","exception":"[object] (Exception(code: 0): Ho-ho-ho exception has been raised at /var/www/app/Http/Controllers/API/BaseController.php:24) [stacktrace] ...'
     * ]
     */
    protected function write(array $record): void
    {
        // Just use specified custom error handler
        $this->errorHandler->handleError($record);
    }
}