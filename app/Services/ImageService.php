<?php

namespace App\Services;

use Intervention\Image\Image;
use Intervention\Image\ImageManager;
use \Illuminate\Support\Facades\File;

use App\Exceptions\ImageException;

/**
 * Сервис работы с изображениями
 */
class ImageService
{
    /**
     * @const Root assets directory
     */
    const ASSETS_DIR = 'resources/assets/';

    /**
     * @const Root public directory
     */
    const PUBLIC_DIR = 'public/';

    /**
     * @const Base uploaded image extension
     */
    const BASE_EXTENSION = 'jpg';

    /** @const Base uploads directory */
    const UPLOAD_DIR = 'resources/assets/static/uploads/';

    /**
     * Sanitize filename
     *
     * @param string $string
     * @param bool $forceLowercase
     * @param bool $extra
     * @return string
     */
    protected function sanitizeFilename(string $string, bool $forceLowercase = true, bool $extra = false) : string
    {
        $strip = ["~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]",
            "}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
            "â€”", "â€“", ",", "<", ".", ">", "/", "?"];
        $clean = trim(str_replace($strip, "", strip_tags($string)));
        $clean = preg_replace('/\s+/', "-", $clean);
        $clean = ($extra) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean ;
        if ($forceLowercase) {
            $clean = mb_strtolower($clean, 'UTF-8');
        }

        return $clean;
    }

    /**
     * Генерация внутреннего имени загруженного файла с токеном файла
     *
     * @param string $filename
     * @param string $path
     * @return string
     */
    protected function createUniqueFilename(string $filename, $path = null) : string
    {
        $uploadPath = $path !== null ? $path : env('UPLOAD_PATH');
        $fullImagePath = $uploadPath . $filename . '.jpg';
        if (File::exists($fullImagePath)) {
            // Generate token for image
            $imageToken = substr(sha1(mt_rand()), 0, 5);

            return $filename . '-' . $imageToken;
        }

        return $filename;
    }

    /**
     * Get base filename without extension
     *
     * @param string $filename
     * @param string $extension
     * @return string
     */
    protected function getFilenameWithoutExtension(string $filename, $extension = null) : string
    {
        $extension = $extension !== null ? $extension : File::extension($filename);

        return substr(
            $filename,
            0,
            strlen($filename) - ($extension ? strlen($extension) + 1 : 0)
        );
    }

    /**
     * Get image's content from base64-encoded
     *
     * @param string $content   Base64-encoded file content
     * @return mixed
     * @throws ImageException
     */
    public function getContentFromBase64(string $content)
    {
        $content = explode('base64,', $content);
        if (!$content || count($content) != 2 || !$content[1]) {

            throw new ImageException('Bad base64 image data!');
        }

        return base64_decode($content[1]);
    }

    /**
     * Get file token by it's content
     *
     * @param $content
     * @return string
     */
    public function getTokenByContent($content) : string
    {
        return substr(sha1($content), 0, 7);
    }

    /**
     * Загрузка изображения
     *
     * @param mixed $photo
     * @return Image
     */
    public function uploadImage($photo)
    {
        $originalFilename = $photo->getClientOriginalName();
        $extension = File::extension($originalFilename);
        $originalNameWithoutExt = $this->getFilenameWithoutExtension($originalFilename, $extension);
        $filename = $this->sanitizeFilename($originalNameWithoutExt);
        $manager = new ImageManager();

        return $manager->make($photo)
            ->encode($extension)
            ->save(
                base_path(static::UPLOAD_DIR) .
                $this->createUniqueFilename($filename) .
                '.' . $extension
            );
    }

    /**
     * Crop and compress picture
     *
     * @param array $params Array like
     * [
     *      'imgUrl'   => string,
     *      'imgW'     => int,
     *      'imgH'     => int,
     *      'rotation' => float,
     *      'width'    => int,
     *      'height'   => int,
     *      'imgX1'    => int,
     *      'imgY1'    => int,
     * ]
     * @return Image
     */
    public function cropImage(array $params)
    {
        $manager = new ImageManager();
        $image = $manager->make($params['imgUrl']);
        $image->resize($params['imgW'], $params['imgH'])
            ->rotate( - $params['rotation'])
            ->crop($params['width'], $params['height'], $params['imgX1'], $params['imgY1'])
            ->save(base_path(static::UPLOAD_DIR) . 'cropped-' . File::basename($params['imgUrl']));

        return $image;
    }

    /**
     * Forming path - add '/' at end and base path to start
     * Works with linux root / dir only!
     *
     * @param string $path
     * @return string
     */
    protected function formPath(string $path) : string
    {
        if ($path && substr($path, strlen($path) - 1, 1) != DIRECTORY_SEPARATOR) {
            $path .= DIRECTORY_SEPARATOR;
        }
        if (substr($path, 0, 1) != DIRECTORY_SEPARATOR) {
            $path = base_path($path);
        }

        return $path;
    }

    /**
     * Copy file 'filename' from 'from' to 'to'
     *
     * @param string $filename
     * @param string $from
     * @param string $to
     * @throws ImageException
     */
    public function copy(string $filename, string $from, string $to)
    {
        $to = $this->formPath($to);
        // If file already exists, go exit
        if (File::exists($to . $filename)) {

            return;
        }
        $from = $this->formPath($from);
        if (!copy($from . $filename, $to . $filename)) {

            throw new ImageException("Can't copy image '$filename' from '$from' to '$to'");
        }
    }

    /**
     * Create image by it's content, filename and path
     *
     * @param string $content   File content
     * @param string $filename  New filename. Will be processed by $this->sanitizeFilename()
     * @param string $path      Relative file path. Will be extended to absolute path
     * @return string
     * @throws ImageException
     */
    public function createFromContent(string $content, string $filename, string $path)
    {
        $path = $this->formPath($path);
        if (!File::exists($path)) {

            throw new ImageException("Image path '$path' does not exists!");
        }
        // Get file token and try to find the same file in this directory
        $token = $this->getTokenByContent($content);
        $extension = File::extension($filename);
        $sameFile = $this->findByToken($token, $extension, $path);
        if ($sameFile !== false) {

            // Don't create alike files
            return $sameFile;
        }
        // Get sanitized filename with file token
        $basename = $this->getFilenameWithoutExtension($filename, $extension);
        $filename = $this->sanitizeFilename($basename)
            . '_' . $token
            . '.' . $extension;
        // Write content
        if (File::put($path . $filename, $content) === false) {

            throw new ImageException("Can't write '$filename' content in '$path'");
        }

        return $filename;
    }

    /**
     * Search file in specified token by it's token
     *
     * @param $token
     * @param $extension
     * @param $path
     * @return string|false
     */
    public function findByToken(string $token, string $extension, string $path)
    {
        $path = $this->formPath($path);
        if (!$files = glob($path . "*_$token.$extension")) {

            return false;
        }
        // Return first found file, not directories
        // Really it can be only one file with one token in directory
        foreach ($files as $file) {
            if (File::isFile($file)) {
                return File::basename($file);
            }
        }

        return false;
    }

    /**
     * Resize asset image to 'size' height and copy to '/size' subdirectory
     *
     * @param string $filename
     * @param string $path
     * @param int $height
     * @throws ImageException
     */
    public function heightenAsset(string $filename, string $path, int $height)
    {
        $path = $this->formPath($path);
        // Copy file to /h{height} directory
        $resPath = $path . 'h' .$height . DIRECTORY_SEPARATOR;
        if (!File::exists($resPath)) {
            File::makeDirectory($resPath, 493, true);
        }
        $this->copy($filename, $path, $resPath);
        $image = (new ImageManager())->make($resPath . $filename)
            ->heighten($height, function ($constraint) {
                $constraint->upsize();
            })
            ->save();
        if (!$image) {

            throw new ImageException("Can't heighten image '{$path}{$filename}!");
        }
    }

    /**
     * Resize asset image to 'size' height and copy to '/size' subdirectory
     *
     * @param string $filename
     * @param string $path
     * @param int $width
     * @throws ImageException
     */
    public function widenAsset(string $filename, string $path, int $width)
    {
        $path = $this->formPath($path);
        // Copy file to /w{width} directory
        $resPath = $path . 'w' .$width . DIRECTORY_SEPARATOR;
        if (!File::exists($resPath)) {
            File::makeDirectory($resPath, 493, true);
        }
        $this->copy($filename, $path, $resPath);
        $image = (new ImageManager())->make($resPath . $filename)
            ->widen($width, function ($constraint) {
                $constraint->upsize();
            })
            ->save();
        if (!$image) {

            throw new ImageException("Can't heighten image '{$path}{$filename}!");
        }
    }

    /**
     * Save asset and it's resized miniatures
     *
     * @param $content
     * @param string $filename
     * @param string $path
     * @param array $resizeOn Sizes to resize. Array like
     * [
     *      'heighten' => [int, int, int],
     *      'widen'    => [int, int, int],
     *      'resize'   => [??],
     * ]
     * @return string Saved filename
     * @throws ImageException
     */
    public function saveAsset($content, string $filename, string $path, array $resizeOn = []) : string
    {
        // Get raw file content and save it ot static path
        $filename = $this->createFromContent(
            $this->getContentFromBase64($content),
            $filename,
            static::ASSETS_DIR . $path
        );
        // Copy file to public
        $this->copy(
            $filename,
            static::ASSETS_DIR . $path,
            static::PUBLIC_DIR . $path
        );
        // Create specified miniatures
        if (isset($resizeOn['heighten']) && is_array($resizeOn['heighten'])) {
            foreach ($resizeOn['heighten'] as $size) {
                $this->heightenAsset($filename, static::PUBLIC_DIR . $path, $size);
            }
        }
        if (isset($resizeOn['widen']) && is_array($resizeOn['widen'])) {
            foreach ($resizeOn['widen'] as $size) {
                $this->widenAsset($filename, static::PUBLIC_DIR . $path, $size);
            }
        }

        return $filename;
    }
}