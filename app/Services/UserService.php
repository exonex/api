<?php

namespace App\Services;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Models\User;
use App\Traits\ReCaptcha;

/**
 * Сервис пользователей
 */
class UserService
{
    // Проверка капчи
    use ReCaptcha;

    /**
     * @public User Модель авторизованного пользователя
     */
    public $user;

    /**
     * @param User|null $user
     */
    public function __construct(User $user = null)
    {
        $this->user = $user;
    }

    /**
     * Авторизация пользователя по email и паролю
     * В случае невозможности авторизоваться вызывается исключение
     *
     * @param array $input
     * @throws ModelNotFoundException|ValidationException
     */
    public function login(array $input)
    {
        $validator = Validator::make($input, [
            'email'    => 'required|email',
            'password' => 'required|min:6|max:32',
        ]);
        if ($validator->fails()) {

            throw new ValidationException($validator);
        }
        $success = Auth::attempt(
            [
                'email'    => $input['email'],
                'password' => $input['password']
            ],
            isset($input['remember']) && $input['remember'] === 'on'
        );
        if (!$success) {

            throw new ModelNotFoundException();
        }
    }
}