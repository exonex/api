<?php

namespace App\Services\Mailers;

use Mail;

/**
 * Базовый хелпер отправки электронной почты
 * @todo Сделать отправку почты через нормальные очереди
 */
abstract class Mailer
{
    /**
     * Отправка почты
     * 
     * @param string $email     Адрес отправки
     * @param string $subject   Тема письма
     * @param string $fromEmail Адресат отправки
     * @param string $view      Имя представления с шаблоном письма
     * @param array $data       Массив данных для представления
     */
    public function sendTo(string $email, string $subject, string $fromEmail, string $view, array $data = [])
    {
        Mail::queue(
            $view,
            $data,
            function ($message) use ($email, $subject, $fromEmail)
            {
                /** @var $message */
                $message->from($fromEmail, env('MAIL_USERNAME'));
                $message->to($email)->subject($subject);
            }
        );
    }
}