<?php

namespace App\Services\Crud;

use Illuminate\Database\Eloquent\Model;

use App\Models\ProductCompany;
use App\Services\ImageService;
use App\Exceptions\ImageException;
use App\Exceptions\NotAllowedException;

/**
 * Product companies CRUD service
 */
class ProductCompanyService extends BaseCrudService
{
    /**
     * @const Logos base dir
     */
    const LOGO_DIR = 'static/img/logo/';

    /**
     * Base model class
     *
     * @const string|ProductCompany
     */
    const MODEL_CLASS = ProductCompany::class;

    /**
     * Fields to save in 'save' action
     *
     * @var array
     */
    protected $saveFields = ['id', 'countryId', 'name', 'nameEn', 'description', 'logo', 'site', 'order'];

    /**
     * Fields to select in 'read' action
     *
     * @var array
     */
    protected $readFields = ['id', 'countryId', 'name', 'nameEn', 'description', 'logo', 'site', 'order'];

    /**
     * Fields to select in 'list' action
     *
     * @var array
     */
    protected $listFields = ['id', 'countryId', 'name', 'nameEn', 'description', 'logo', 'site', 'order'];

    /**
     * Fields to order in 'list' action
     *
     * @var array
     */
    protected $orderFields = ['name', 'id'];

    /**
     * Sizes on what logo will be resized
     *
     * @var array
     */
    protected $logoHeights = [16, 24, 32, 48, 64, 128, 256];

    /**
     * @var ImageService
     */
    protected $imageService = null;

    /**
     * ProductCompany CRUD service constructor.
     *
     * @param ImageService $imageService
     */
    public function __construct(ImageService $imageService)
    {
        parent::__construct();
        $this->imageService = $imageService;
    }

    /**
     * Create existing or update new product company.
     * In case of update existing item,
     * $data must have 'id' field or something like that
     *
     * @param array $data
     * @return array|Model Primary key and logo filename
     * @throws ImageException
     */
    public function save(array $data) : Model
    {
        // Save logo if it specified
        $logoName = $data['logo'];
        if (isset($data['logoContent'])) {
            $data['logo'] = $this->imageService
                ->saveAsset(
                    $data['logoContent'],
                    $data['logo'],
                    static::LOGO_DIR,
                    [
                        'heighten' => $this->logoHeights
                    ]
                );
        }

        // Save model and return awaiting data
        /** @var ProductCompany $company */
        $company = parent::save($data);

        // Create company's brand
        if (empty($data['id']) && !empty($data['createBrand'])) {
            /** @var ProductBrandService $brandService */
            $brandService = app(ProductBrandService::class);
            $brandData = [
                'id'          => 0,
                'companyId'   => $company->id,
                'name'        => $company->name,
                'nameEn'      => $company->nameEn,
                'description' => $company->description,
                'order'       => 0,
                'logo'        => $logoName,
            ];
            if (isset($data['logoContent'])) {
                $brandData['logoContent'] = $data['logoContent'];
            }
            /** @var array $brand ID and logo */
            $brand = $brandService->save($brandData);
        }

        return $company;
    }

    /**
     * Delete item
     *
     * @param int|string $id
     * @return bool
     * @throws \Exception
     */
    public function delete($id) : bool
    {
        if (ProductCompany::model()->findOrFail($id)->brands()->count() > 0) {

            throw new NotAllowedException("Cannot delete company with active brand");
        }

        return (bool)ProductCompany::destroy($id);
    }
}