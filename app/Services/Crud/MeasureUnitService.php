<?php

namespace App\Services\Crud;

use App\Models\MeasureUnit;
use App\Services\Crud\Traits\CrudSaveNotAllowed;
use App\Services\Crud\Traits\CrudDeleteNotAllowed;

/**
 * Measure units CRUD service
 */
class MeasureUnitService extends BaseCrudService
{
    // Reject edit actions
    use CrudDeleteNotAllowed, CrudSaveNotAllowed;

    /**
     * Base model class
     *
     * @const string|MeasureUnit
     */
    const MODEL_CLASS = MeasureUnit::class;

    /**
     * Fields to select in 'read' action
     *
     * @var array
     */
    protected $readFields = ['id', 'categoryId', 'name', 'designation', 'code', 'codeStr', 'parentId', 'ratio', 'order'];

    /**
     * Fields to select in 'list' action
     *
     * @var array
     */
    protected $listFields = ['id', 'categoryId', 'name', 'designation', 'code', 'codeStr', 'parentId', 'ratio', 'order'];
}