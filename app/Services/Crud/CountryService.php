<?php

namespace App\Services\Crud;

use App\Models\Country;
use App\Services\Crud\Traits\CrudSaveNotAllowed;
use App\Services\Crud\Traits\CrudDeleteNotAllowed;

/**
 * Countries CRUD service
 */
class CountryService extends BaseCrudService
{
    // Reject edit actions
    use CrudDeleteNotAllowed, CrudSaveNotAllowed;

    /**
     * Base model class
     *
     * @const string|Country
     */
    const MODEL_CLASS = Country::class;

    /**
     * Fields to select in 'read' action
     *
     * @var array
     */
    protected $readFields = ['id', 'id3', 'name', 'fullName', 'nameEn', 'code', 'phoneCode', 'domain', 'order'];

    /**
     * Fields to select in 'list' action
     *
     * @var array
     */
    protected $listFields = ['id', 'id3', 'name', 'fullName', 'nameEn', 'code', 'phoneCode', 'domain', 'order'];

    /**
     * Fields to order in 'list' action
     *
     * @var array
     */
    protected $orderFields = ['name' => 'asc'];
}