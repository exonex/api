<?php

namespace App\Services\Crud;

use App\Models\Base\Model;
use Illuminate\Database\Eloquent\Model;

use App\Models\Base\Node;
use App\Models\ProductCategory;
use Illuminate\Validation\ValidationException;

/**
 * Product categories CRUD service
 */
class ProductCategoryService extends BaseCrudService
{
    /**
     * Base model class
     *
     * @const string|ProductCategory
     */
    const MODEL_CLASS = ProductCategory::class;

    /**
     * Fields to save in 'save' action
     *
     * @var array
     */
    protected $saveFields = ['id', 'name', 'pathName', 'path', 'description', 'measures', 'parentId', 'order'];

    /**
     * Fields to select in 'read' action
     *
     * @var array
     */
    protected $readFields = ['id', 'name', 'pathName', 'path', 'description', 'measures', 'parentId', 'order'];

    /**
     * Fields to select in 'list' action
     *
     * @var array
     */
    protected $listFields = ['id', 'name', 'pathName', 'path', 'description', 'measures', 'parentId', 'order'];

    /**
     * Get roots list
     *
     * @param array|null $fields
     * @return array
     */
    public function rootsList(array $fields = null)
    {
        $roots = ProductCategory::roots()
            ->get($fields ?: $this->listFields)
            ->toArray();

        return $this->updateChildrenCount($roots);
    }

    /**
     * Get child nodes list by root ID
     *
     * @param int $id
     * @param array|null $fields
     * @return array|\Illuminate\Database\Eloquent\Collection|Model[]
     */
    public function nodesList(int $id, array $fields = null)
    {
        $children = ProductCategory::model()
            ->findOrFail($id)
            ->descendants()
            ->limitDepth(1)
            ->get($fields ?: $this->listFields)
            ->toArray();

        return $this->updateChildrenCount($children);
    }

    /**
     * Updates children count in roots list and return it with ['children' => int] column
     * in every row
     *
     * @param array $nodes Nodes list like [
     *      ['id' => int, . . . ],
     *      . . .
     * ]
     * @return array Updated list like [
     *      ['id' => int, 'children' => int, . . . ],
     *      . . .
     * ]
     */
    protected function updateChildrenCount(array $nodes) : array
    {
        $childrenCount = ProductCategory::model()->getChildrenCount(array_column($nodes, 'id'));
        foreach ($nodes as &$node) {
            $node['children'] = $childrenCount[$node['id']];
        }

        return $nodes;
    }

    /**
     * Get total active categories count
     *
     * @return int
     */
    public function totalCount()
    {
        return ProductCategory::model()->count();
    }

    /**
     * Create existing or update new item.
     * In case of update existing item,0
     * $data must have 'id' field or something like that
     *
     * @param array $data
     * @return Model Created or updated model
     * @throws ValidationException
     * @throws \Exception
     */
    public function save(array $data) : Model
    {
        if (empty($data['parentId']) || $data['parentId'] < 0) {
            $data['parentId'] = null;
        }
        // Получим путь к узлу от корня
        if ($data['parentId']) {
            $parent = ProductCategory::model($data['parentId']);
            $data['path']     = $parent->path . ($parent->path ? Node::PATH_SEPARATOR : '') . $parent->id;
            $data['pathName'] = $parent->pathName . ($parent->pathName ? Node::PATH_SEPARATOR : '') . $parent->name;
        } else {
            $data['path'] = $data['pathName'] = '';
        }

        return parent::save($data);
    }
}