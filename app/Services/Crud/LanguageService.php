<?php

namespace App\Services\Crud;

use App\Models\Language;
use App\Services\Crud\Traits\CrudSaveNotAllowed;
use App\Services\Crud\Traits\CrudDeleteNotAllowed;

/**
 * Languages CRUD service
 */
class LanguageService extends BaseCrudService
{
    // Reject edit actions
    use CrudDeleteNotAllowed, CrudSaveNotAllowed;

    /**
     * Base model class
     *
     * @const string|Language
     */
    const MODEL_CLASS = Language::class;

    /**
     * Fields to select in 'read' action
     *
     * @var array
     */
    protected $readFields = ['id', 'name', 'nameEn', 'iso2', 'iso3', 'gost', 'code', 'order'];

    /**
     * Fields to select in 'list' action
     *
     * @var array
     */
    protected $listFields = ['id', 'name', 'nameEn', 'iso2', 'iso3', 'gost', 'code', 'order'];
}