<?php

namespace App\Services\Crud\Traits;

use App\Exceptions\NotAllowedException;

/**
 * Trait for CRUD services with not allowed 'delete' actions
 *
 * @const string MODEL_CLASS
 */
trait CrudDeleteNotAllowed
{
    /**
     * Reject to delete item
     *
     * @param int|string $id
     * @return bool
     * @throws NotAllowedException
     */
    public function delete($id) : bool
    {
        throw new NotAllowedException("'DELETE' action not allowed in '" . static::MODEL_CLASS . "' CRUD service");
    }
}