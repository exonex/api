<?php

namespace App\Services\Crud\Traits;

use App\Models\Base\Model;
use App\Exceptions\NotAllowedException;

/**
 * Trait for CRUD services with not allowed 'save' actions
 *
 * @const string MODEL_CLASS
 */
trait CrudSaveNotAllowed
{
    /**
     * Reject to save item
     *
     * @param array $data
     * @return Model
     * @throws NotAllowedException
     */
    public function save(array $data) : Model
    {
        throw new NotAllowedException("'SAVE' action not allowed in '" . static::MODEL_CLASS . "' CRUD service");
    }
}