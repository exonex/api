<?php

namespace App\Services\Crud;

use Illuminate\Database\Eloquent\Model;

use App\Models\ProductBrand;
use App\Services\ImageService;
use App\Exceptions\ImageException;

/**
 * Product brands CRUD service
 */
class ProductBrandService extends BaseCrudService
{
    /**
     * @const Logos base dir
     */
    const LOGO_DIR = 'static/img/brand/';

    /**
     * Base model class
     *
     * @const string|ProductBrand
     */
    const MODEL_CLASS = ProductBrand::class;

    /**
     * Fields to save in 'save' action
     *
     * @var array
     */
    protected $saveFields = ['id', 'companyId', 'name', 'nameEn', 'description', 'logo', 'order'];

    /**
     * Fields to select in 'read' action
     *
     * @var array
     */
    protected $readFields = ['id', 'companyId', 'name', 'nameEn', 'description', 'logo', 'order'];

    /**
     * Fields to select in 'list' action
     *
     * @var array
     */
    protected $listFields = ['id', 'companyId', 'name', 'nameEn', 'description', 'logo', 'order'];

    /**
     * Sizes on what logo will be resized
     *
     * @var array
     */
    protected $logoHeights = [16, 24, 32, 48, 64, 128, 256];

    /**
     * @var ImageService
     */
    protected $imageService = null;

    /**
     * ProductBrand CRUD service constructor.
     *
     * @param ImageService $imageService
     */
    public function __construct(ImageService $imageService)
    {
        parent::__construct();
        $this->imageService = $imageService;
    }

    /**
     * Create existing or update new product company.
     * In case of update existing item,
     * $data must have 'id' field or something like that
     *
     * @param array $data
     * @return Model Created or updated model
     * @throws ImageException
     */
    public function save(array $data) : Model
    {
        // Save logo if it specified
        if (isset($data['logoContent'])) {
            $data['logo'] = $this->imageService
                ->saveAsset(
                    $data['logoContent'],
                    $data['logo'],
                    static::LOGO_DIR,
                    [
                        'heighten' => $this->logoHeights
                    ]
                );
        }

        // Save model and return awaiting data
        /** @var ProductBrand $model */
        $model = parent::save($data);

        return $model;
    }

    /**
     * Delete item
     *
     * @param int|string $id
     * @return bool
     * @throws \Exception
     */
    public function delete($id) : bool
    {
        return (bool)ProductBrand::destroy($id);
    }
}