<?php

namespace App\Services\Crud;

use App\Models\State;
use App\Services\Crud\Traits\CrudSaveNotAllowed;
use App\Services\Crud\Traits\CrudDeleteNotAllowed;

/**
 * States CRUD service
 */
class StateService extends BaseCrudService
{
    // Reject edit actions
    use CrudDeleteNotAllowed, CrudSaveNotAllowed;

    /**
     * Base model class
     *
     * @const string|State
     */
    const MODEL_CLASS = State::class;

    /**
     * Fields to select in 'read' action
     *
     * @var array
     */
    protected $readFields = ['id', 'name', 'countryId', 'timeZone', 'order'];

    /**
     * Fields to select in 'list' action
     *
     * @var array
     */
    protected $listFields = ['id', 'name', 'countryId', 'timeZone', 'order'];
}