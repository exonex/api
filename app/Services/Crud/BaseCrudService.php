<?php

namespace App\Services\Crud;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Models\Base\Model;
use App\Models\Base\ExtendedModel;
use App\Contracts\CrudServiceInterface;
use App\Exceptions\CrudServiceException;

/**
 * Base CRUD service class
 */
abstract class BaseCrudService implements CrudServiceInterface
{
    /**
     * Models namespace
     *
     * @const string
     */
    const MODELS_NAMESPACE = 'App\Models';

    /**
     * Base model class
     *
     * @const string|Model
     */
    const MODEL_CLASS = '-';

    /**
     * Primary key field
     *
     * @var string
     */
    protected $pkField = 'id';

    /**
     * Fields to save in 'save' action
     *
     * @var array
     */
    protected $saveFields = [];

    /**
     * Fields to select in 'read' action
     *
     * @var array
     */
    protected $readFields = [];

    /**
     * Fields to select in 'list' action
     *
     * @var array
     */
    protected $listFields = [];

    /**
     * Fields to order in 'list' action
     *
     * @var array
     */
    protected $orderFields = ['order'];

    /**
     * BaseCrudService constructor.
     */
    public function __construct()
    {
        if (!class_exists(static::MODEL_CLASS)) {

            throw new CrudServiceException("Model class '" . static::MODEL_CLASS . "' not found");
        }
    }

    /**
     * Create existing or update new item.
     * In case of update existing item,0
     * $data must have 'id' field or something like that
     *
     * @param array $data
     * @return Model Created or updated model
     * @throws ValidationException
     * @throws CrudServiceException
     */
    public function save(array $data) : Model
    {
        $isNew = empty($data[$this->pkField]) || $data[$this->pkField] < 0;
        // Remove from data unnecessary fields for this action
        $data = array_intersect_key($data, array_flip($this->saveFields));

        /** @var Model|ExtendedModel $modelClass */
        $modelClass = static::MODEL_CLASS;

        // Run model validation and save data
        // Creating new model
        if ($isNew) {
            /** @var Model $model */
            $model = new $modelClass($data);

        // Updating existing model
        } else {
            if (!isset($data[$this->pkField])) {

                throw new CrudServiceException("Primary key \$data[{$this->pkField}] not set");
            }
            /** @var Model $model */
            $model = $modelClass::model()->find($data[$this->pkField]);
            unset($data[$this->pkField]);
            // Чтобы обработать все мутаторы, сеттеры и т.п., присвоим поля через индивидуальный сеттер
            foreach ($data as $key => $value) {
                $model->setAttribute($key, $value);
            }
        }

        // Save updated or created model
        $model->saveOrFail();

        return $model;
    }

    /**
     * Read item's data
     *
     * @param int|string $id
     * @param bool $asArray
     * @return array|Model
     * @throws ModelNotFoundException
     */
    public function read($id, bool $asArray = false)
    {
        /** @var Model $model */
        $model = (static::MODEL_CLASS)::findOrFail($id, $this->readFields);

        return $model && $asArray
            ? $model->toArray()
            : $model;
    }

    /**
     * Return items list in attributes or models array
     *
     * @param bool $asArray = false
     * @param array $fields Selected fields or class property $this->listFields will be selected
     * @return array|Collection|Model[]
     * @throws \Exception
     */
    public function list(bool $asArray = false, array $fields = null)
    {
        /** @var Builder $result */
        $result = (static::MODEL_CLASS)::query();
        foreach ($this->orderFields as $key => $value) {
            $result = is_int($key)
                ? $result->orderBy($value)          // orderBy('column') for $this->orderFields == ['column']
                : $result->orderBy($key, $value);   // orderBy('column', 'desc') for $this->orderFields == ['column' => 'desc']
        }
        /** @var Collection $result */
        $result = $result->get($fields ?: $this->listFields);
        if ($result && $asArray) {
            $result = $result->toArray();
        }

        return $result;
    }

    /**
     * Delete item
     *
     * @param int|string $id
     * @return bool
     * @throws \Exception
     */
    public function delete($id) : bool
    {
        return (bool)(static::MODEL_CLASS)::destroy($id);
    }
}