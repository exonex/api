<?php

namespace App\Services\Crud;

use App\Models\MeasureCategory;
use App\Services\Crud\Traits\CrudSaveNotAllowed;
use App\Services\Crud\Traits\CrudDeleteNotAllowed;

/**
 * Measure categories CRUD service
 */
class MeasureCategoryService extends BaseCrudService
{
    // Reject edit actions
    use CrudDeleteNotAllowed, CrudSaveNotAllowed;

    /**
     * Base model class
     *
     * @const string|MeasureCategory
     */
    const MODEL_CLASS = MeasureCategory::class;

    /**
     * Fields to select in 'read' action
     *
     * @var array
     */
    protected $readFields = ['id', 'name', 'order'];

    /**
     * Fields to select in 'list' action
     *
     * @var array
     */
    protected $listFields = ['id', 'name', 'order'];
}