<?php

namespace App\Traits;

use App\Models\Base\Model;
use DateInterval;
use Carbon\Carbon;

/**
 * Трейт для определения возраста и срока годности объекта
 * Использует указанное свойство для вычисления возраста, или $this->created_at, если не передавать параметров
 *
 * @property Carbon  $created_at
 */
trait Age {
    /**
     * Возраст объекта
     *
     * @param string $field    Имя свойства, по которому считается возраст. По умолчанию created_at
     * @param string $endField Имя свойства, задающего конец временного интервала. По умолчанию текущее время
     * @return bool|DateInterval
     */
    public function age(string $field = Model::CREATED_AT, $endField = null)
    {
        return (new Carbon($this->$field))->diff(new Carbon($endField ? $this->$endField : 'now'));
    }
    
    /**
     * Возраст объекта в полных часах
     *
     * @param string $field Имя свойства, по которому считается возраст. По умолчанию created_at
     * @param string $endField Имя свойства, задающего конец временного интервала. По умолчанию текущее время
     * @return int
     */
    public function ageInHours(string $field = Model::CREATED_AT, $endField = null)
    {
        return (new Carbon($this->$field))->diffInHours(new Carbon($endField ? $this->$endField : 'now'));
    }

    /**
     * Возраст объекта в полных сутках
     *
     * @param string $field Имя свойства, по которому считается возраст. По умолчанию created_at
     * @param string $endField Имя свойства, задающего конец временного интервала. По умолчанию текущее время
     * @return int
     */
    public function ageInDays(string $field = Model::CREATED_AT, $endField = null)
    {
        return (new Carbon($this->$field))->diffInDays(new Carbon($endField ? $this->$endField : 'now'));
    }
}