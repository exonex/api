<?php

namespace App\Traits;

use Illuminate\Http\JsonResponse;

/**
 * Returning controller's action result as JSONed array like this:
 *
 *     [
 *          'success' => true,
 *          'data'    => mixed,     // optional
 *     ]
 *
 *    or
 *
 *     [
 *          'success' => false,
 *          'code'    => int,
 *          'error'   => string,
 *     ]
 *
 * Class JsonApiResponse
 * @package App\Traits
 */
trait JsonApiResponse
{
    /**
     * Send request config flag
     *
     * @var bool
     */
    protected $jsonApiResponseSendConfig = false;

    /**
     * Get request config
     *
     * @return array
     */
    protected function getRequestConfig() : array
    {
        // @todo Execution time, db queries count, memory used
        return [
            'method' => request()->method(),
            'url'    => request()->url(),
            'data'   => request()->all(),
        ];
    }

    /**
     * Forming success response
     *
     * @param mixed $data
     * @return JsonResponse
     */
    protected function jsonSuccess($data) : JsonResponse
    {
        $result = [
            'success'    => true,
            'status'     => 200,
            'statusText' => 'OK'
        ];
        if (func_num_args() > 0) {
            $result['data'] = $data;
        }
        if ($this->jsonApiResponseSendConfig) {
            $result['config'] = $this->getRequestConfig();
        }

        return response()->json($result, 200, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Forming error response
     *
     * @param string|array $error
     * @param int          $code
     * @return JsonResponse
     */
    protected function jsonError($error, int $code = 200) : JsonResponse
    {
        $result = [
            'success' => false,
            'error'   => $error,
            'status'  => $code,
        ];
        if ($this->jsonApiResponseSendConfig) {
            $result['config'] = $this->getRequestConfig();
        }

        return response()->json($result, $code, [], JSON_UNESCAPED_UNICODE);
    }
}