<?php

namespace App\Traits;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ChildrenCount
 *
 * Получение числа детей для каждого из переданных узлов дерева
 *
 * @property integer $id
 *
 * @package App\Traits
 */
trait ChildrenCount
{
    /**
     * Получение числа детей для каждого из переданных узлов дерева
     *
     * @param array $ids Список ID узлов
     * @param bool $withTrashed Флаг учитывая удалённых записей
     * @return array Array like this
     * [
     *      ['id' => 1, 'childrenCount' => 1],
     *      ['id' => 2, 'childrenCount' => 10],
     *      ['id' => 3, 'childrenCount' => 0],
     *      . . .
     * ]
     */
    public function getChildrenCount(array $ids = null, $withTrashed = false) : array
    {
        $result = static::query()
            ->selectRaw('p.id AS id, COUNT(c.id) AS "childrenCount"')
            ->from(DB::raw('"' . $this->getTable() . '" p'))
            ->leftJoin(DB::raw('"' . $this->getTable() . '" c'), DB::raw('p.id'), '=', DB::raw('c."' . $this->parentColumn. '"'))
            ->groupBy(DB::raw('p.id'))
            ->whereIn(DB::raw('p.id'), $ids ?: [$this->id]);
        if (!$withTrashed && in_array(SoftDeletes::class, class_uses_recursive(static::class))) {
            $result = $result
                ->whereRaw('p."' . static::DELETED_AT . '" IS NULL AND c."' . static::DELETED_AT . '" IS NULL')
                ->withTrashed();
        }
        $result = $result->get()
            ->toArray();
        return array_column($result, 'childrenCount', 'id');
    }
}