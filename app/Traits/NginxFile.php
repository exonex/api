<?php

namespace App\Traits;

/**
 * Методы отдачи файлов через Nginx
 */
trait NginxFile
{
    /**
     * Отдача содержимого файла
     *
     * @param string $filename  Полное имя файла без ведущего /
     * @return bool             Существование файла, а значит и успешность отдачи
     */
    public static function nginxGive(string $filename)
    {
        if (!file_exists(base_path($filename))) {

            return false;
        }
        // В отличие от отдачи через PHP, тут нужен лидирующий /
        header('X-Accel-Redirect: ' . DIRECTORY_SEPARATOR . $filename);

        return true;
    }

    /**
     * Отдача файла на скачивание
     *
     * @param string $filename  Полное имя файла без ведущего /
     * @param string $fileTitle Имя файла для клиента
     * @return bool             Существование файла $fullFilename, а значит и успешность отдачи
     */
    public static function nginxDownload(string $filename, string $fileTitle)
    {
        if (!static::nginxGive($filename)) {

            return false;
        }
        $fullFilename = base_path($filename);
        header('Content-Type: ' . finfo_file(finfo_open(FILEINFO_MIME_TYPE), $fullFilename));
        header('Content-Length: ' . filesize($fullFilename));
        header("Content-Disposition: attachment; filename=$fileTitle");

        return true;
    }
}