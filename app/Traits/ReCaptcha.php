<?php

namespace App\Traits;

use \Illuminate\Support\Facades\Input;
use ReCaptcha\ReCaptcha as ReCaptchaClass;

use App\Exceptions\CaptchaFailsException;

/**
 * Трейт для работы с рекапчей от Google
 */
trait ReCaptcha
{
    /**
     * Проверка капчи от Google
     * @return bool
     */
    public function checkCaptcha() : bool
    {
        $reCaptcha = new ReCaptchaClass(env('RE_CAP_SECRET'));
        $resp = $reCaptcha->verify(
            Input::get('g-recaptcha-response'),
            $_SERVER['REMOTE_ADDR'] ?? ''
        );
        return $resp->isSuccess();
    }

    /**
     * Проверка капчи от Google
     * В случае неудачи вызывается исключение CaptchaFailException
     * @throws CaptchaFailsException
     */
    public function checkCaptchaOrFail()
    {
        if (!$this->checkCaptcha()) {
            throw new CaptchaFailsException();
        }
    }
}