/*
 |-------------------------------------------------------------------------------
 | app.js
 |-------------------------------------------------------------------------------
 | Main file
 */

// Libs linking
require('./lib');

require('./config');

/**
 * Vue instance initialization
 */
import Vue from 'vue';
import router from './routes.js'

// Laravel Passport Vue components
import Clients from './components/passport/Clients.vue'
import AuthorizedClients from './components/passport/AuthorizedClients.vue'
import PersonalAccessTokens from './components/passport/PersonalAccessTokens.vue'

// Register Passport components
Vue.component('passport-clients', Clients);
Vue.component('passport-authorized-clients', AuthorizedClients);
Vue.component('passport-personal-access-tokens', PersonalAccessTokens);

// Create new instance
new Vue({router})
    .$mount('#app');
