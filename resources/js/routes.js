/*
 |-------------------------------------------------------------------------------
 | routes.js
 |-------------------------------------------------------------------------------
 | Contains all of the routes for the application
 */

// Imports Vue and VueRouter to extend with the routes.
import Vue from 'vue'
import VueRouter from 'vue-router'

// Extends Vue to use Vue Router
Vue.use( VueRouter );

// Makes a new VueRouter that we will use to run all of the routes for the app
export default new VueRouter({
    routes: [
        {
            path: '/cp',
            name: 'dashboard',
            component: Vue.component( 'Dashboard', require( './pages/Dashboard.vue' ) )
        },
        {
            path: '/profile',
            name: 'profile',
            component: Vue.component( 'Profile', require( './pages/Profile.vue' ) )
        },
        {
            path: '/contacts',
            name: 'contacts',
            component: Vue.component( 'Profile', require( './pages/Contacts.vue' ) )
        }
    ]
});