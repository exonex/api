/**
 * Common regular expressions
 */
export const RegEx = {
    // Domain without protocol, leading 'www.' and ending '/'
    MASK_DOMAIN: '(([a-zа-я]{1,2})|([a-zа-я][0-9])|([0-9][a-zа-я])|([a-zа-я0-9][a-zа-я0-9-_]{1,61}[a-zа-я0-9]))\\.([a-zа-я]{2,6}|[a-zа-я0-9-]{2,30}\\.[a-zа-я]{2,3})',

    // Net protocol with '://'
    MASK_PROTOCOL: '(http|https|ftp)://',

    // Host path like '/path/path/' with or without leading or ending '/'
    MASK_PATH: '/?([a-z0-9_\\-]+/)*',

    /**
     * Checking domain
     *
     * @param {string} str
     * @returns {boolean}
     */
    isSite (str) {
        let exp = new RegExp("^(" + this.MASK_PROTOCOL + ")?(www\\.)?" + this.MASK_DOMAIN + "$", 'i');
        return exp.test(str);
    },

    /**
     * Checking domain
     *
     * @param {string} str
     * @returns {boolean}
     */
    isDomain (str) {
        let exp = new RegExp("^(www\\.)?" + this.MASK_DOMAIN + "$", 'i');
        return exp.test(str);
    },

    /**
     * Checking domain
     *
     * @param {string} str
     * @returns {boolean}
     */
    isUrl (str) {
        let exp = new RegExp("^(" + this.MASK_PROTOCOL + ")?(www\\.)?" + this.MASK_DOMAIN + this.MASK_PATH + "$", 'i');
        return exp.test(str);
    }
};

