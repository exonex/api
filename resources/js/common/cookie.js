/**
 * Cookies helper
 */
export const Cookie = {
    /**
     * Get cookie with specified name, or undefined if it does not exists
     *
     * @param {string} name
     * @returns {string}
     */
    get (name) {
        let matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    },

    /**
     * Check specified cookie existing
     *
     * @param {string} name
     * @returns {boolean}
     */
    has (name) {
        return Boolean(document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        )));
    },

    /**
     * Set cookie with specified parameters
     *
     * @param {string} name
     * @param {string} value
     * @param {Object} options Object with listed properties:
     *      {Number|Date} expires - Cookie expire interval from now in seconds or Date object.
     *           Number - time in seconds before expire
     *           Date – date of expiration.
     *           If expires is in past, cookie will be deleted
     *           If expires does not specified, or 0, cookie will be set as session and will be deleted after browser close
     *      {string} path - Cookie path.
     *      {string} domain - Cookie domain.
     *      {boolean} secure - If true, cookie will be sent over secured connection
     */
    set (name, value, options = {}) {
        options = options || {};
        let expires = options.expires;

        if (typeof expires == "number" && expires) {
            let d = new Date();
            d.setTime(d.getTime() + expires * 1000);
            expires = options.expires = d;
        }

        if (expires && expires.toUTCString) {
            options.expires = expires.toUTCString();
        }

        value = encodeURIComponent(value);
        let updatedCookie = name + "=" + value;

        for (let propName in options) {
            updatedCookie += "; " + propName;
            let propValue = options[propName];
            if (propValue !== true) {
                updatedCookie += "=" + propValue;
            }
        }

        document.cookie = updatedCookie;
    },

    /**
     * Delete specified cookie
     *
     * @param {string} name
     */
    delete (name) {
        this.set(name, "", {
            expires: -1
        })
    }
};

