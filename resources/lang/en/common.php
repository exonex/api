<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Common Language Lines
    |--------------------------------------------------------------------------
    */

    'main' => 'Main',
    'menu' => 'Menu',
    'errors_occurred' => 'Errors occurred',
    'control_panel' => 'Control panel',
    'index_page' => 'It\'s a main page',
    'contacts' => 'Team contacts',

];