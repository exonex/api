<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'log_in' => 'Log in',
    'log_out' => 'Log out',
    'authorization' => 'Authorization',
    'password' => 'Password',
    'remember_me' => 'Remember me',
    'success_logout' => 'Logged out',
    'user_not_found' => 'User not found',
    'access_denied' => 'Access denied',

];
