@extends('layouts.base')

@section('content')
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">@lang('common.contacts')</div>
            <div class="panel-body">
                Tel: 8-800-555-35-35 - It's easier to call than borrow from someone
            </div>
        </div>
    </div>
@endsection