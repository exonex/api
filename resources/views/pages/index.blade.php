@extends('layouts.base')

@section('content')

    @if(Session::has('message'))

        <div class=" col-md-10 col-md-offset-1 alert @if(Session::has('status'))alert-{{ Session::get('status') }}@else alert-info @endif">
            {{ Session::get('message') }}
        </div>

    @endif

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">@lang('common.index_page')</div>
            <div class="panel-body">
                Hello, world!
            </div>
        </div>
    </div>
@endsection