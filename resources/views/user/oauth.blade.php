@extends('user.profile')

@section('title')
    @lang('user.oauth')
@endsection

@section('profile-content')
    <passport-clients></passport-clients>
    <passport-authorized-clients></passport-authorized-clients>
    <passport-personal-access-tokens></passport-personal-access-tokens>
@endsection

