<?php
/**
 * @var \App\Models\User $user
 */
?>
@extends('layouts.base')

@section('title')
    @lang('user.profile')
@endsection

@section('profile-content')
    Hello, {{ $user->name }}! Here is your personal profile
@endsection

@section('content')
    <div class="col-md-1">
        <ul>
            <li><a href="{{ route('user.profile') }}">@lang('user.profile')</a></li>
            <li><a href="{{ route('user.oauth') }}">@lang('user.oauth')</a></li>
        </ul>
    </div>
    <div class="col-md-11">
        <div class="panel panel-default">
            <div class="panel-heading">@yield('title')</div>
            <div class="panel-body">
                @yield('profile-content')
            </div>
        </div>
    </div>
@endsection