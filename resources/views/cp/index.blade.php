@extends('layouts.base')

@section('content')
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">@lang('common.control_panel')</div>
            <div class="panel-body">
                <router-view></router-view>
                @yield('cp-content')
            </div>
        </div>
    </div>
@endsection

