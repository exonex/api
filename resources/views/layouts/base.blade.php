<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script type='text/javascript'>
        window.Laravel = "<?= csrf_token() ?>"
    </script>
    <link rel="icon" type="image/x-icon" href="/favicon.ico">

    <title>Exo Goods</title>

    <link href="{{ asset('css/lib.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    @yield('head')
</head>
<body>
    @include('layouts.components.navbar')

    <div id="app" class="container">
        @yield('content')
    </div>

    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>