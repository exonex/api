<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">

            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">@lang('common.menu')</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <a class="navbar-brand" href="/">
                @lang('common.main')
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <ul class="nav navbar-nav">
                @if(!Auth::guest() && Auth::user()->isAdmin())
                <li><a href="{{ route('cp.index') }}">@lang('common.control_panel')</a></li>
                @endif
                <li><a href="{{ route('pages.contacts') }}">@lang('contacts.contacts')</a></li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                @if (Auth::guest())
                <li>
                    <a href="{{ route('auth.login') }}">@lang('auth.log_in')</a>
                </li>
                @else
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ route('user.profile') }}"><i class="fa fa-btn fa-user"></i>@lang('user.profile')</a></li>
                        <li><a href="{{ route('auth.logout') }}"><i class="fa fa-btn fa-sign-out"></i>@lang('auth.log_out')</a></li>
                    </ul>
                </li>
                @endif
            </ul>
        </div>
    </div>
</nav>