<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        // Простые, ни от кого не зависящие таблицы
        $this->call(Seeders\UserRoleSeeder::class);
        $this->call(Seeders\MeasureCategorySeeder::class);
        $this->call(Seeders\LanguageSeeder::class);
        $this->call(Seeders\CountrySeeder::class);

        // Таблицы с зависимостями только от простых таблиц
        $this->call(Seeders\StatesSeeder::class);
        $this->call(Seeders\MeasureUnitSeeder::class);
        // Сидер категорий продуктов пока заполняет дерево без измерений
        $this->call(Seeders\ProductCategorySeeder::class);

        // Все остальные таблицы
        $this->call(Seeders\UserSeeder::class);
        $this->call(Seeders\ProductCompanySeeder::class);
        $this->call(Seeders\ProductBrandSeeder::class);

        Model::reguard();
    }
}
