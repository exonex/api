<?php

namespace Seeders;

use Illuminate\Database\Seeder;

use App\Models\Language;

class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $languages = [
            ['Avar', 'Аварский', 'av', 'ava', 'ava', 'ава', '014'],
            ['Adyghe', 'Адыгейский', 'ad', 'ady', 'ady', 'ады', '020'],
            ['Azerbaijani', 'Азербайджанский', 'az', 'aze', 'aze', 'азе', '025'],
            ['Albanian', 'Албанский', 'sq', 'alb', 'sqi', 'алб', '030'],
            ['English', 'Английский', 'en', 'eng', 'eng', 'анг', '045'],
            ['Arabic', 'Арабский', 'ar', 'ara', 'ara', 'ара', '050'],
            ['Armenian', 'Армянский', 'hy', 'arm', 'hye', 'арм', '055'],
            ['Bashkir', 'Башкирский', 'ba', 'bak', 'bak', 'баш', '086'],
            ['Belarusian', 'Белорусский', 'be', 'bel', 'bel', 'бел', '090'],
            ['Bulgarian', 'Болгарский', 'bg', 'bul', 'bul', 'бол', '115'],
            ['Buryat', 'Бурятский', 'bu', 'bua', 'bua', 'бур', '125'],
            ['Welsh', 'Валлийский', 'cy', 'cym', 'cym', 'вал', '130'],
            ['Hungarian', 'Венгерский', 'hu', 'hun', 'hun', 'вен', '133'],
            ['Vietnamese', 'Вьетнамский', 'vi', 'vie', 'vie', 'вье', '140'],
            ['Greek', 'Греческий', 'el', 'gre', 'ell', 'гре', '157'],
            ['Georgian', 'Грузинский', 'ka', 'geo', 'kat', 'гру', '158'],
            ['Dargin', 'Даргинский', 'dr', 'dar', 'dar', 'даг', '175'],
            ['Danish', 'Датский', 'da', 'dan', 'dan', 'дат', '178'],
            ['Hebrew', 'Иврит', 'he', 'heb', 'heb', 'ивр', '198'],
            ['Ingush', 'Ингушский', 'in', 'inh', 'inh', 'инг', '205'],
            ['Irish', 'Ирландский', 'ga', 'gle', 'gle', 'ирл', '220'],
            ['Icelandic', 'Исландский', 'is', 'ice', 'isl', 'исл', '225'],
            ['Spanish', 'Испанский', 'es', 'spa', 'spa', 'исп', '230'],
            ['Italian', 'Итальянский', 'it', 'ita', 'ita', 'ита', '235'],
            ['Kabardian', 'Кабардино-черкесский', 'kb', 'kbd', 'kbd', 'каа', '250'],
            ['Kazakh', 'Казахский', 'kk', 'kaz', 'kaz', 'каз', '255'],
            ['Kalmyk', 'Калмыцкий', 'xa', 'xal', 'xal', 'кал', '260'],
            ['Karachay-Balkar', 'Карачаево-балкарский', 'ky', 'krc', 'krc', 'као', '280'],
            ['Kyrgyz', 'Киргизский', 'ki', 'kir', 'kir', 'кир', '305'],
            ['Chinese', 'Китайский', 'zh', 'chi', 'zho', 'кит', '315'],
            ['Komi', 'Коми', 'kv', 'kom', 'kom', 'кои', '320'],
            ['Korean', 'Корейский', 'ko', 'kor', 'kor', 'коо', '330'],
            ['Kumyk', 'Кумыкский', 'ku', 'kum', 'kum', 'кум', '349'],
            ['Lak', 'Лакский', 'lb', 'lbe', 'lbe', 'лак', '370'],
            ['Latvian', 'Латышский', 'lv', 'lav', 'lav', 'лаш', '385'],
            ['Lezgin', 'Лезгинский', 'le', 'lez', 'lez', 'лез', '390'],
            ['Lithuanian', 'Литовский', 'lt', 'lit', 'lit', 'лит', '400'],
            ['Macedonian', 'Македонский', 'mk', 'mac', 'mkd', 'маа', '415'],
            ['Mari', 'Марийский', 'cm', 'chm', 'chm', 'мач', '445'],
            ['Moldavian', 'Молдавский', 'mo', 'mol', 'mol', 'мол', '460'],
            ['Moksha', 'Мокшанский', 'md', 'mdf', 'mdf', 'мок', '455'],
            ['Mongolian', 'Монгольский', 'mn', 'mon', 'mon', 'мон', '463'],
            ['German', 'Немецкий', 'de', 'ger', 'deu', 'нем', '481'],
            ['Dutch', 'Нидерландский', 'nl', 'nld', 'nld', 'нид', '495'],
            ['Norwegian', 'Норвежский', 'no', 'nor', 'nor', 'нор', '506'],
            ['Ossetian', 'Осетинский', 'os', 'oss', 'oss', 'ост', '524'],
            ['Persian', 'Персидский', 'fa', 'per', 'fas', 'пер', '535'],
            ['Polish', 'Польский', 'pl', 'pol', 'pol', 'пол', '540'],
            ['Portuguese', 'Португальский', 'pt', 'por', 'por', 'пор', '545'],
            ['Romanian', 'Румынский', 'ro', 'rum', 'ron', 'рум', '565'],
            ['Russian', 'Русский', 'ru', 'rus', 'rus', 'рус', '570'],
            ['Serbian', 'Сербский', 'sr', 'scc', 'srp', 'срб', '-'],
            ['Slovak', 'Словацкий', 'sk', 'slk', 'slk', 'сло', '605'],
            ['Slovenian', 'Словенский', 'sl', 'slv', 'slv', 'слв', '610'],
            ['Swahili', 'Суахили', 'sw', 'swa', 'swa', 'суа', '631'],
            ['Tabasaran', 'Табасаранский', 'ta', 'tab', 'tab', 'таб', '635'],
            ['Tajik', 'Таджикский', 'tg', 'tgk', 'tgk', 'тад', '640'],
            ['Thai', 'Тайский', 'th', 'tha', 'tha', 'таи', '645'],
            ['Tatar', 'Татарский', 'tt', 'tat', 'tat', 'тар', '660'],
            ['Tuvan', 'Тувинский', 'ty', 'tyv', 'tyv', 'тув', '690'],
            ['Turkish', 'Турецкий', 'tr', 'tur', 'tur', 'тур', '693'],
            ['Turkman', 'Туркменский', 'tk', 'tuk', 'tuk', 'тук', '695'],
            ['Udmurt', 'Удмуртский', 'ud', 'udm', 'udm', 'удм', '700'],
            ['Uzbek', 'Узбекский', 'uz', 'uzb', 'uzb', 'узб', '710'],
            ['Ukrainian', 'Украинский', 'uk', 'ukr', 'ukr', 'укр', '720'],
            ['Finnish', 'Финский', 'fi', 'fin', 'fin', 'фин', '740'],
            ['French', 'Французский', 'fr', 'fra', 'fra', 'фра', '745'],
            ['Hindi', 'Хинди', 'hi', 'hin', 'hin', 'хин', '770'],
            ['Croatian', 'Хорватский', 'hr', 'hrv', 'hrv', 'хор', '-'],
            ['Romany', 'Цыганский', 'rm', 'rom', 'rom', 'цыг', '780'],
            ['Chechen', 'Чеченский', 'ce', 'che', 'che', 'чеч', '785'],
            ['Czech', 'Чешский', 'cs', 'cze', 'ces', 'чеш', '790'],
            ['Chuvash', 'Чувашский', 'cv', 'chv', 'chv', 'чув', '795'],
            ['Swedish', 'Шведский', 'sv', 'swe', 'swe', 'шве', '805'],
            ['Scots', 'Шотландский', 'sc', 'sco', 'sco', 'шот', '-'],
            ['Esperanto', 'Эсперанто', 'eo', 'epo', 'epo', 'эсп', '845'],
            ['Erzya', 'Эрзянский', 'my', 'myv', 'myv', 'эрз', '835'],
            ['Estonian', 'Эстонский', 'et', 'est', 'est', 'эст', '850'],
            ['Yakut', 'Якутский', 'sa', 'sah', 'sah', 'яку', '865'],
            ['Japanese', 'Японский', 'ja', 'jpn', 'jpn', 'япо', '870'],
        ];

        foreach ($languages as $i => $language) {
            Language::model()->create([
                'id'        => $language[2],
                'name'      => $language[1],
                'nameEn'    => $language[0],
                'iso2'      => $language[3],
                'iso3'      => $language[4],
                'gost'      => $language[5],
                'code'      => $language[6],
                'order'     => $i,
            ]);
        }
    }
}
