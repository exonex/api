<?php

use Illuminate\Database\Seeder;

use App\Models\LogApiCallRule;

/**
 * Class LogApiCallRuleSeeder
 *
 * Log API calls logging rules seeder
 */
class LogApiCallRuleSeeder extends Seeder
{
    protected $rules = [
        ['*', '*', '*', '*', '*', '*', 1, 'Базовое правило - всем разрешаем'],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->rules as $rule) {
            LogApiCallRule::model()->create([
                'userId'  => $rule[0],
                'type'    => $rule[1],
                'code'    => $rule[2],
                'action'  => $rule[3],
                'tag'     => $rule[4],
                'ip'      => $rule[5],
                'save'    => $rule[6],
                'comment' => $rule[7],
            ]);
        }
    }
}
