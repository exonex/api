<?php

namespace Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use App\Models\User;
use App\Models\Country;
use App\Models\UserRole;

/**
 * Сидер пользователей
 */
class UserSeeder extends Seeder
{
    use UpdatePkSequence;

    /**
     * Get DB table name
     *
     * @return string
     */
    protected function getTable() : string
    {
        return 'users';
    }

    public function run(){
        DB::table($this->getTable())->delete();

        $countryId = Country::RUSSIA;

        // Пробный админ
        /** @var User $user */
        $user = User::model()->create([
            'id'            => 1,
            'name'          => 'admin',
            'lastName'      => 'admin',
            'gender'        => User::GENDER_UNDEFINED,
            'birthdate'     => '1900-01-01',
            'avatar'        => '',
            'nickname'      => 'admin',
            'email'         => 'admin@admin.com',
            'countryId'     => Country::USA,
            'password'      => Hash::make('password'),
            'phone'         => '8-800-555-35-35',
        ]);
        $user->assignRole(UserRole::USER);
        $user->assignRole(UserRole::ADMIN);

        // Первые пользователи
        $user = User::model()->create([
            'id'            => 2,
            'name'          => 'Виктор',
            'lastName'      => 'Фурсенко',
            'gender'        => User::GENDER_MALE,
            'birthdate'     => '1988-04-07',
            'nickname'      => 'Billy Noname',
            'email'         => 'viktor@admin.com',
            'countryId'     => Country::RUSSIA,
            'password'      => Hash::make('123456'),
        ]);
        $user->assignRole(UserRole::USER);
        $user->assignRole(UserRole::CUSTOMER);
        $user->assignRole(UserRole::ADMIN);

        $user = User::model()->create([
            'id'            => 3,
            'name'          => 'Роман',
            'lastName'      => 'Киевский',
            'gender'        => User::GENDER_MALE,
            'nickname'      => 'Loki',
            'email'         => 'roma@admin.com',
            'countryId'     => $countryId,
            'password'      => Hash::make('123456'),
        ]);
        $user->assignRole(UserRole::USER);
        $user->assignRole(UserRole::CUSTOMER);
        $user->assignRole(UserRole::ADMIN);

        $user = User::model()->create([
            'id'            => 4,
            'name'          => 'Константин',
            'lastName'      => 'Костоглодов',
            'gender'        => User::GENDER_MALE,
            'nickname'      => 'Patrikap',
            'email'         => 'kostya@admin.com',
            'countryId'     => $countryId,
            'password'      => Hash::make('123456'),
        ]);
        $user->assignRole(UserRole::USER);
        $user->assignRole(UserRole::CUSTOMER);
        $user->assignRole(UserRole::ADMIN);

        // Пробные пользователи с разными группами
        $user = User::model()->create([
            'id'            => 5,
            'name'          => 'user',
            'gender'        => User::GENDER_UNDEFINED,
            'nickname'      => 'user',
            'email'         => 'user@user.com',
            'countryId'     => $countryId,
            'password'      => Hash::make('123456'),
        ]);
        $user->assignRole(UserRole::USER);

        $user = User::model()->create([
            'id'            => 6,
            'name'          => 'customer',
            'gender'        => User::GENDER_FEMALE,
            'nickname'      => 'customer',
            'email'         => 'customer@customer.com',
            'countryId'     => $countryId,
            'password'      => Hash::make('123456'),
        ]);
        $user->assignRole(UserRole::USER);
        $user->assignRole(UserRole::CUSTOMER);

        $user = User::model()->create([
            'id'            => 7,
            'name'          => 'moderator',
            'gender'        => User::GENDER_FEMALE,
            'nickname'      => 'moderator',
            'email'         => 'moderator@moderator.com',
            'countryId'     => $countryId,
            'password'      => Hash::make('123456'),
        ]);
        $user->assignRole(UserRole::USER);
        $user->assignRole(UserRole::MODERATOR);

        $user = User::model()->create([
            'id'            => 8,
            'name'          => 'banned',
            'gender'        => User::GENDER_UNDEFINED,
            'nickname'      => 'banned',
            'email'         => 'banned@banned.com',
            'countryId'     => $countryId,
            'password'      => Hash::make('123456'),
        ]);
        $user->assignRole(UserRole::USER);
        $user->assignRole(UserRole::BANNED);

        // Update PostgreSQL PK sequence with incremented max saved ID
        $this->updatePkSequence();
    }
}