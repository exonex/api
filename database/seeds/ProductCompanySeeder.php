<?php

namespace Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use App\Models\Country;
use App\Models\ProductCompany;

/**
 * Сидер таблицы фирм
 */
class ProductCompanySeeder extends Seeder
{
    use UpdatePkSequence;

    /**
     * Get DB table name
     *
     * @return string
     */
    protected function getTable() : string
    {
        return 'productCompanies';
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table($this->getTable())->delete();

        $companies = [
            [1, Country::RUSSIA,   'Яндекс',   'Yandex',   'Российский поисковик',   'yandex_c2b1ee6.png',   'yandex.ru'],
            [2, Country::USA,      'Google',   'Google',   'Американский поисковик', 'google_74be06a.png',   'google.com'],
            [3, 'KR',              'Samsung',  'Samsung',  'Южнокорейская компания', 'samsung_d017f71.png',  'samsung.com'],
            [4, 'KR',              'LG',       'LG',       'Южнокорейская компания', 'lg_3058a3e.png',       'lg.com'],
        ];

        foreach ($companies as $i => $company) {
            ProductCompany::model()->create([
                'id'            => $company[0],
                'countryId'     => $company[1],
                'name'          => $company[2],
                'nameEn'        => $company[3],
                'description'   => $company[4],
                'logo'          => $company[5],
                'site'          => $company[6],
                'order'         => 0,
            ]);
        }

        // Update PostgreSQL PK sequence with incremented max saved ID
        $this->updatePkSequence();
    }
}
