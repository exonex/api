<?php

namespace Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use App\Models\UserRole;

/**
 * Сидер ролей пользователей
 */
class UserRoleSeeder extends Seeder
{
    public function run(){
        DB::table('userRoles')->delete();

        UserRole::model()->create([
            'id'    => UserRole::ADMIN,
            'name'  => 'Администратор',
            'order' => 0,
        ]);

        UserRole::model()->create([
            'id'    => UserRole::MODERATOR,
            'name'  => 'Модератор',
            'order' => 1,
        ]);

        UserRole::model()->create([
            'id'    => UserRole::CUSTOMER,
            'name'  => 'Клиент',
            'order' => 2,
        ]);

        UserRole::model()->create([
            'id'    => UserRole::USER,
            'name'  => 'Пользователь',
            'order' => 100,
        ]);

        UserRole::model()->create([
            'id'    => UserRole::BANNED,
            'name'  => 'Забанен',
            'order' => 200,
        ]);
    }
}