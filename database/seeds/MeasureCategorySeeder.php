<?php

namespace Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use App\Models\MeasureCategory;

/**
 * Сидер групп единиц измерения
 */
class MeasureCategorySeeder extends Seeder
{
    public function run(){
        DB::table('measureCategories')->delete();
        $data = [
            [MeasureCategory::LENGTH,    'Длина',                0],
            [MeasureCategory::AREA,      'Площадь',              1],
            [MeasureCategory::VOLUME,    'Объём',                2],
            [MeasureCategory::WEIGHT,    'Масса',                3],
            [MeasureCategory::LIGHT,     'Свет',                 4],
            [MeasureCategory::TEMP,      'Температура',          5],
            [MeasureCategory::SPEED,     'Скорость',             6],
            [MeasureCategory::FREQUENCY, 'Частота',              7],
            [MeasureCategory::TIME,      'Время',                8],
            [MeasureCategory::FORCE,     'Сила',                 9],
            [MeasureCategory::POWER,     'Мощность',             10],
            [MeasureCategory::ENERGY,    'Энергия',              11],
            [MeasureCategory::PRESSURE,  'Давление',             12],
            [MeasureCategory::ELECTRIC,  'Электричество',        13],
            [MeasureCategory::CURRENCY,  'Валюта',               14],
            [MeasureCategory::DATA,      'Данные',               15],
            [MeasureCategory::OTHER,     'Прочие',               16],
        ];

        foreach ($data as $item) {
            MeasureCategory::model()->create([
                'id'    => $item[0],
                'name'  => $item[1],
                'order' => $item[2],
            ]);
        }
    }
}