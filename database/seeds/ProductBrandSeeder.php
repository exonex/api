<?php

namespace Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use App\Models\ProductBrand;
use App\Models\ProductCompany;

/**
 * Сидер таблицы брендов
 */
class ProductBrandSeeder extends Seeder
{
    use UpdatePkSequence;

    /**
     * Get DB table name
     *
     * @return string
     */
    protected function getTable() : string
    {
        return 'productBrands';
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table($this->getTable())->delete();

        $brands = [
            [1, ProductCompany::whereName('Samsung')->firstOrFail()->id, 'Samsung', 'Samsung', 'Южнокорейская компания', 'samsung_d017f71.png'],
            [2, ProductCompany::whereName('LG')->firstOrFail()->id,      'LG',      'LG',      'Южнокорейская компания', 'lg_3058a3e.png'],
        ];

        foreach ($brands as $i => $company) {
            ProductBrand::model()->create([
                'id'            => $company[0],
                'companyId'     => $company[1],
                'name'          => $company[2],
                'nameEn'        => $company[3],
                'description'   => $company[4],
                'logo'          => $company[5],
                'order'         => 0,
            ]);
        }

        // Update PostgreSQL PK sequence with incremented max saved ID
        $this->updatePkSequence();
    }
}
