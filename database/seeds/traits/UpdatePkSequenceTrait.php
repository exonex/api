<?php

namespace Seeders;

use DB;

/**
 * Update table PK sequence wit max saved PK
 */
trait UpdatePkSequence
{
    /**
     * Get DB table name
     *
     * @return string
     */
    abstract protected function getTable() : string;

    /**
     * Update table PK sequence wit max saved PK
     *
     * @param string $pkField
     */
    protected function updatePkSequence(string $pkField = 'id')
    {
        $maxId = DB::selectOne("SELECT MAX($pkField) FROM \"{$this->getTable()}\"")->max + 1;
        DB::statement("ALTER SEQUENCE \"{$this->getTable()}_id_seq\" RESTART WITH $maxId");
    }
}