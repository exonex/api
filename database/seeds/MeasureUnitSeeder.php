<?php

namespace Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use App\Models\MeasureUnit;
use App\Models\MeasureCategory;

/**
 * Сидер единиц измерения
 * @see https://ru.wikipedia.org/wiki/%D0%95%D0%B4%D0%B8%D0%BD%D0%B8%D1%86%D1%8B_%D1%84%D0%B8%D0%B7%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B8%D1%85_%D0%B2%D0%B5%D0%BB%D0%B8%D1%87%D0%B8%D0%BD
 */
class MeasureUnitSeeder extends Seeder
{
    public function run(){
        DB::table('measureUnits')->delete();
        $data = [
            // Длина
            ['m',    'Метр',      'м',    MeasureCategory::LENGTH, '006', 'MTR'],
            ['nm',   'Нанометр',  'нм',   MeasureCategory::LENGTH, '001', 'NNM', 'm', 0.000000001],
            ['µm',   'Микрометр', 'мкм',  MeasureCategory::LENGTH, '002', 'MKM', 'm', 0.000001],
            ['mm',   'Миллиметр', 'мм',   MeasureCategory::LENGTH, '003', 'MMT', 'm', 0.001],
            ['cm',   'Сантиметр', 'см',   MeasureCategory::LENGTH, '004', 'CMT', 'm', 0.01],
            ['dm',   'Дециметр',  'дм',   MeasureCategory::LENGTH, '005', 'DMT', 'm', 0.1],
            ['km',   'Километр',  'км',   MeasureCategory::LENGTH, '008', 'KMT', 'm', 1000],
            ['in',   'Дюйм',      'дюйм', MeasureCategory::LENGTH, '039', 'INH', 'm', 0.0254],
            ['ft',   'Фут',       'фут',  MeasureCategory::LENGTH, '041', 'FOT', 'm', 0.3048],
            ['yd',   'Ярд',       'ярд',  MeasureCategory::LENGTH, '043', 'YRD', 'm', 0.9144],
            ['mile', 'Миля',      'миль', MeasureCategory::LENGTH, '045', 'SMI', 'm', 1609.34],
            // Площадь, сечение
            ['m2',    'Квадратный метр',      'м2',    MeasureCategory::AREA, '055', 'MTK'],
            ['mm2',   'Квадратный миллиметр', 'мм2',   MeasureCategory::AREA, '050', 'MMK', 'm2', 0.000001],
            ['cm2',   'Квадратный сантиметр', 'см2',   MeasureCategory::AREA, '051', 'CMK', 'm2', 0.0001],
            ['dm2',   'Квадратный дециметр',  'дм2',   MeasureCategory::AREA, '053', 'DMK', 'm2', 0.01],
            ['a',     'Ар',                   'сот',   MeasureCategory::AREA, '109', 'ARE', 'm2', 100],
            ['ha',    'Гектар',               'га',    MeasureCategory::AREA, '059', 'HAR', 'm2', 10000],
            ['km2',   'Квадратный километр',  'км2',   MeasureCategory::AREA, '061', 'KMK', 'm2', 1000000],
            ['in2',   'Квадратный дюйм',      'дюйм2', MeasureCategory::AREA, '071', 'INK', 'm2', 0.00064516],
            ['ft2',   'Квадратный фут',       'фут2',  MeasureCategory::AREA, '073', 'FTK', 'm2', 0.092903],
            ['yd2',   'Квадратный ярд',       'ярд2',  MeasureCategory::AREA, '075', 'YDK', 'm2', 0.836126999997],
            ['acre',  'Акр',                  'акр',   MeasureCategory::AREA, '077', 'ACR', 'm2', 4046.8546799855],
            ['mile2', 'Квадратная миля',      'миль2', MeasureCategory::AREA, '079', 'MIC', 'm2', 2590000],
            // Объём
            ['m3',     'Кубический метр',      'м3',     MeasureCategory::VOLUME, '113', 'MTQ'],
            ['mm3',    'Кубический миллиметр', 'мм3',    MeasureCategory::VOLUME, '110', 'MMQ', 'm3', 0.000000001],
            ['cm3',    'Кубический сантиметр', 'см3',    MeasureCategory::VOLUME, '111', 'CMQ', 'm3', 0.000001],
            ['ml',     'Миллилитр',            'мл',     MeasureCategory::VOLUME, '111', 'MLT', 'm3', 0.000001],
            ['dm3',    'Кубический дециметр',  'дм3',    MeasureCategory::VOLUME, '112', 'DMQ', 'm3', 0.001],
            ['L',      'Литр',                 'л',      MeasureCategory::VOLUME, '112', 'LTR', 'm3', 0.001],
            ['in3',    'Кубический дюйм',      'дюйм3',  MeasureCategory::VOLUME, '131', 'INQ', 'm3', 0.0000163871],
            ['ft3',    'Кубический фут',       'фут3',   MeasureCategory::VOLUME, '132', 'FTQ', 'm3', 0.0283168],
            ['yd3',    'Кубический ярд',       'ярд3',   MeasureCategory::VOLUME, '133', 'YDQ', 'm3', 0.764555],
            ['barrel', 'Баррель',              'барр.',  MeasureCategory::VOLUME, '146', 'BLL', 'm3', 0.158987],
            ['fl oz',  'Унция США',            'унц',    MeasureCategory::VOLUME, '141', 'OZA', 'm3', 0.000029574],
            ['gal',    'Галлон',               'галл',   MeasureCategory::VOLUME, '145', 'GLL', 'm3', 0.00378541],
            ['qt',     'Кварта',               'кварта', MeasureCategory::VOLUME, '144', 'QTL', 'm3', 0.000946353],
            ['pt',     'Пинта',                'пинта',  MeasureCategory::VOLUME, '143', 'PTL', 'm3', 0.000473176],
            // Масса
            ['kg',   'Килограмм',      'кг',     MeasureCategory::WEIGHT, '166', 'KGM'],
            ['mg',   'Миллиграмм',     'мг',     MeasureCategory::WEIGHT, '161', 'MGM', 'kg', 0.000001],
            ['g',    'Грамм',          'г',      MeasureCategory::WEIGHT, '163', 'GRM', 'kg', 0.001],
            ['t',    'Тонна',          'т',      MeasureCategory::WEIGHT, '168', 'TNE', 'kg', 1000],
            ['kt',   'Килотонна',      'кт',     MeasureCategory::WEIGHT, '170', 'KTN', 'kg', 1000000],
            ['q',    'Центнер',        'ц',      MeasureCategory::WEIGHT, '206', 'DTN', 'kg', 100],
            ['MC',   'Карат',          'кар',    MeasureCategory::WEIGHT, '162', 'CTM', 'kg', 0.0002],
            ['lb',   'Фунт',           'фунт',   MeasureCategory::WEIGHT, '186', 'LBR', 'kg', 0.453592],
            ['oz',   'Унция',          'унц',    MeasureCategory::WEIGHT, '187', 'ONZ', 'kg', 0.0283495],
            ['apoz', 'Тройская унция', 'тр.унц', MeasureCategory::WEIGHT, '201', 'APZ', 'kg', 0.0311035],
            // Температура
            ['°C',   'Градус Цельсия',    '°C', MeasureCategory::TEMP, '280', 'CEL'],
            ['°F',   'Градус Фаренгейта', '°F', MeasureCategory::TEMP, '281', 'FAN'],
            ['°K',   'Кельвин',           '°К', MeasureCategory::TEMP, '288', 'KEL'],
            // Свет
            ['cd',   'Кандела', 'кд', MeasureCategory::LIGHT, '282', 'CDL'],
            ['lx',   'Люкс',    'лк', MeasureCategory::LIGHT, '283', 'LUX'],
            ['lm',   'Люмен',   'лм', MeasureCategory::LIGHT, '284', 'LUM'],
            // Сила
            ['N',    'Ньютон',     'Н',  MeasureCategory::FORCE, '289', 'NEW'],
            ['kN',   'Килоньютон', 'кН', MeasureCategory::FORCE, '',    '',    'N', 1000],
            ['MN',   'Меганьютон', 'МН', MeasureCategory::FORCE, '',    '',    'N', 1000000],
            // Мощность
            ['W',    'Ватт',                 'Вт',     MeasureCategory::POWER, '212', 'WTT'],
            ['kW',   'Киловатт',             'кВт',    MeasureCategory::POWER, '214', 'KWT', 'W', 1000],
            ['MW',   'Мегаватт',             'МВт',    MeasureCategory::POWER, '215', 'MAW', 'W', 1000000],
            ['GW',   'Гигаватт',             'ГВт',    MeasureCategory::POWER, '216', 'GAW', 'W', 1000000000],
            ['hp',   'Лошадиная сила',       'лс',     MeasureCategory::POWER, '251', '',    'W', 735.49875],
            ['khp',  'Тысяча лошадиных сил', 'тыс лс', MeasureCategory::POWER, '252', '',    'W', 735498.75],
            // Энергия
            ['W.h',  'Ватт-час',     'Вт.ч',  MeasureCategory::ENERGY, '243', 'WHR'],
            ['kW.h', 'Киловатт-час', 'кВт.ч', MeasureCategory::ENERGY, '245', 'KWH', 'W.h', 1000],
            ['MW.h', 'Мегаватт-час', 'МВт.ч', MeasureCategory::ENERGY, '246', 'MWH', 'W.h', 1000000],
            ['J',    'Джоуль',       'Дж',    MeasureCategory::ENERGY, '271', 'JOU'],
            ['kJ',   'Килоджоуль',   'кДж',   MeasureCategory::ENERGY, '271', '',    'J', 1000],
            ['MJ',   'Мегаджоуль',   'МДж',   MeasureCategory::ENERGY, '271', '',    'J', 1000000],
            ['cal',  'Калория',      'кал',   MeasureCategory::ENERGY, '284', 'LUM', 'J', 4.1868],
            ['kcal', 'Килокалория',  'ккал',  MeasureCategory::ENERGY, '232', 'LUM', 'J', 4186.8],
            ['Gcal', 'Гигакалория',  'Гкал',  MeasureCategory::ENERGY, '233', '',    'J', 4186800000],
            // Электричество
            ['Om',   'Ом',              'Ом',   MeasureCategory::ELECTRIC, '274', 'OHM'],
            ['kOm',  'Килоом',          'кОм',  MeasureCategory::ELECTRIC, '',    '', 'Om', 1000],
            ['MOm',  'Мегаом',          'МОм',  MeasureCategory::ELECTRIC, '',    '', 'Om', 1000000],
            ['V',    'Вольт',           'В',    MeasureCategory::ELECTRIC, '222', 'VLT'],
            ['kV',   'Киловольт',       'кВ',   MeasureCategory::ELECTRIC, '223', 'KVT', 'V', 1000],
            ['MV',   'Мегавольт',       'МВ',   MeasureCategory::ELECTRIC, '224', 'MVT', 'V', 1000000],
            ['V.A',  'Вольт-ампер',     'В.А',  MeasureCategory::ELECTRIC, '226', 'VAM'],
            ['kV.A', 'Киловольт-ампер', 'КВ.А', MeasureCategory::ELECTRIC, '227', 'KVA', 'V.A', 1000],
            ['MV.A', 'Мегавольт-ампер', 'МВ.А', MeasureCategory::ELECTRIC, '228', 'MVA', 'V.A', 1000000],
            ['A',    'Ампер',           'А',    MeasureCategory::ELECTRIC, '260', 'AMP'],
            ['mA',   'Миллиампер',      'мА',   MeasureCategory::ELECTRIC, '',    '', 'A', 0.001],
            ['A.h',  'Ампер-час',       'А.ч',  MeasureCategory::ELECTRIC, '263', 'AMH'],
            ['mA.h', 'Миллиампер-час',  'мА.ч', MeasureCategory::ELECTRIC, '263', '', 'A.h', 0.001],
            // Давление
            ['Pa',     'Паскаль',                           'Па',       MeasureCategory::PRESSURE, '294', 'PAL'],
            ['kPa',    'Килопаскаль',                       'кПа',      MeasureCategory::PRESSURE, '297', 'KPA', 'Pa', 1000],
            ['MPa',    'Мегапаскаль',                       'МПа',      MeasureCategory::PRESSURE, '298', 'MPA', 'Pa', 1000000],
            ['bar',    'Бар',                               'бар',      MeasureCategory::PRESSURE, '',    '',    'Pa', 100000],
            ['at',     'Атмосфера',                         'ат',       MeasureCategory::PRESSURE, '301', 'ATT', 'Pa', 98066.5],
            ['mm Hg',  'Миллиметр ртутного столба',         'мм рт.ст', MeasureCategory::PRESSURE, '338', '',    'Pa', 133.3223684],
            ['kg/cm2', 'Килограмм на квадратный сантиметр', 'кг/см2',   MeasureCategory::PRESSURE, '317', '',    'Pa', 98066.5],
            ['kg/m2',  'Килограмм на квадратный метр',      'кг/м2',    MeasureCategory::PRESSURE, '',    '',    'Pa', 9.80665],
            // Скорость, ускорение
            ['m/s',    'Метр в секунду',             'м/с',    MeasureCategory::SPEED, '328', 'MTS'],
            ['km/h',   'Километр в час',             'км/ч',   MeasureCategory::SPEED, '333', 'KMH', 'm/s', 0.277778],
            ['mile/h', 'Миль в час',                 'миль/ч', MeasureCategory::SPEED, '',    '',    'm/s', 0.44704],
            ['kn',     'Узел',                       'уз',     MeasureCategory::SPEED, '327', 'KNT', 'm/s', 0.514],
            ['r/s',    'Оборот в секунду',           'об/с',   MeasureCategory::SPEED, '330', 'RPS'],
            ['r/min',  'Оборот в минуту',            'об/мин', MeasureCategory::SPEED, '331', 'RPM', 'r/s', 60],
            ['kg/s',   'Килограмм в секунду',        'кг/с',   MeasureCategory::SPEED, '499', 'KGS'],
            ['m/s2',   'Метр на секунду в квадрате', 'м/с2',   MeasureCategory::SPEED, '335', 'MSK'],
            ['m3/s',   'Кубический метр в секунду',  'м3/с',   MeasureCategory::SPEED, '596', 'MQS'],
            ['L/s',    'Литр в секунду',             'л/с',    MeasureCategory::SPEED, '', '',       'm3/s', 0.001],
            ['L/min',  'Литр в минуту',              'л/мин',  MeasureCategory::SPEED, '', '',       'm3/s', 0.06],
            ['L/h',    'Литр в час',                 'л/ч',    MeasureCategory::SPEED, '', '',       'm3/s', 3.6],
            ['m3/h',   'Кубический метр в час',      'м3/ч',   MeasureCategory::SPEED, '598', 'MQH', 'm3/s', 3600],
            // Частота
            ['Hz',     'Герц',       'Гц',  MeasureCategory::FREQUENCY, '290', 'HTZ'],
            ['kHz',    'Килогерц',   'кГц', MeasureCategory::FREQUENCY, '291', 'KHZ', 'Hz', 1000],
            ['MHz',    'Мегагерц',   'МГц', MeasureCategory::FREQUENCY, '292', 'MHZ', 'Hz', 1000000],
            ['GHz',    'Гигагагерц', 'ГГц', MeasureCategory::FREQUENCY, '292', 'GHZ', 'Hz', 1000000000],
            // Время
            ['s',     'Секунда',      'с',   MeasureCategory::TIME, '354', 'SEC'],
            ['µs',    'Микросекунда', 'мкс', MeasureCategory::TIME, '352', '',    's', 0.000001],
            ['ms',    'Миллисекунда', 'млс', MeasureCategory::TIME, '353', '',    's', 0.001],
            ['min',   'Минута',       'мин', MeasureCategory::TIME, '355', 'MIN', 's', 60],
            ['h',     'Час',          'ч',   MeasureCategory::TIME, '356', 'HUR', 's', 3600],
            ['d',     'Сутки',        'сут', MeasureCategory::TIME, '359', 'DAY', 's', 86400],
            ['week',  'Неделя',       'нед', MeasureCategory::TIME, '360', 'WEE', 's', 604800],
            ['month', 'Месяц',        'мес', MeasureCategory::TIME, '362', 'МЕС', 's', 18144000],
            ['year',  'Год',          'год', MeasureCategory::TIME, '366', 'ANN', 's', 6622560000],
            // Финансы
            ['rub', 'Рубль',             'руб',   MeasureCategory::CURRENCY, '643', 'RUB'],
            ['Br',  'Белорусский рубль', 'Б.руб', MeasureCategory::CURRENCY, '933', 'BYN'],
            ['₴',   'Гривна',            'грн',   MeasureCategory::CURRENCY, '980', 'UAH'],
            ['₸',   'Тенге',             'тнг',   MeasureCategory::CURRENCY, '',    'TNG'],
            ['$',   'Доллар США',        '$',     MeasureCategory::CURRENCY, '',    'USD'],
            ['€',   'Евро',              '€',     MeasureCategory::CURRENCY, '',    'EUR'],
            ['£',   'Фунт стерлингов',   '£',     MeasureCategory::CURRENCY, '',    'GBP'],
            ['¥',   'Йена',              '¥',     MeasureCategory::CURRENCY, '',    'JPY'],
            // Информация
            ['bit',    'Бит',               'бит',    MeasureCategory::DATA, '254', ''],
            ['kbit',   'Килобит',           'Кбит',   MeasureCategory::DATA, '',    '', 'bit', 1000],
            ['Mbit',   'Мегабит',           'Мбит',   MeasureCategory::DATA, '',    '', 'bit', 1000000],
            ['Gbit',   'Гигабит',           'Гбит',   MeasureCategory::DATA, '',    '', 'bit', 1000000000],
            ['kbit/s', 'Килобит в секунду', 'Кбит/с', MeasureCategory::DATA, '',    ''],
            ['Mbit/s', 'Мегабит в секунду', 'Мбит/с', MeasureCategory::DATA, '',    '', 'kbit/s', 1000],
            ['Gbit/s', 'Гигабит в секунду', 'Гбит/с', MeasureCategory::DATA, '',    '', 'kbit/s', 1000000],
            ['B',      'Байт',              'Б',      MeasureCategory::DATA, '255', ''],
            ['KB',     'Килобайт',          'Кбайт',  MeasureCategory::DATA, '256', '', 'B', 1024],
            ['MB',     'Мегабайт',          'Мбайт',  MeasureCategory::DATA, '257', '', 'B', 1048576],
            ['GB',     'Гигабайт',          'Гбайт',  MeasureCategory::DATA, '',    '', 'B', 1073741824],
            ['TB',     'Терабайт',          'Тбайт',  MeasureCategory::DATA, '',    '', 'B', 1099511627776],
            // Прочие
            ['l.',      'Лист',             'л.',      MeasureCategory::OTHER, '625', ''],
            ['sign',    'Знак',             'знак',    MeasureCategory::OTHER, '922', ''],
            ['word',    'Слово',            'слов',    MeasureCategory::OTHER, '923', ''],
            ['char',    'Символ',           'символ',  MeasureCategory::OTHER, '924', ''],
            ['place',   'Место',            'мест',    MeasureCategory::OTHER, '698', ''],
            ['channel', 'Канал',            'канал',   MeasureCategory::OTHER, '661', ''],
            ['unit',    'Единица',          'ед',      MeasureCategory::OTHER, '642', ''],
            ['%',       'Процент',          '%',       MeasureCategory::OTHER, '744', ''],
            ['dose',    'Доза',             'доз',     MeasureCategory::OTHER, '639', ''],
            ['person',  'Человек',          'чел',     MeasureCategory::OTHER, '792', ''],
            ['set',     'Комплект',         'компл',   MeasureCategory::OTHER, '839', ''],
            ['n. unit', 'Условная единица', 'усл. ед', MeasureCategory::OTHER, '876', ''],
        ];

        foreach ($data as $order => $item) {
            MeasureUnit::model()->create([
                'id'          => $item[0],
                'name'        => $item[1],
                'designation' => $item[2],
                'categoryId'  => $item[3],
                'code'        => $item[4],
                'codeStr'     => $item[5],
                'parentId'    => isset($item[6]) ? $item[6] : null,
                'ratio'       => isset($item[7]) ? $item[7] : 1,
                'order'       => $order,
            ]);
        }
    }
}