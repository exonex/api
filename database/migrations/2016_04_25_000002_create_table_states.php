<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableStates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('states');
        Schema::create('states', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->comment('Название региона');
            $table->char('countryId', 2)->comment('ID страны');
            $table->tinyInteger('timeZone')->default(0)->comment('Разница во времени в часах со временем в Москве');
            $table->char('code', 3)->nullabel()->default('')->comment('Внутренний код региона');
            $table->smallInteger('order')->unsigned()->default(0)->comment('Порядок сортировки');


            $table->unique(['name', 'countryId']);
            $table->foreign('countryId')->references('id')->on('countries')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('states');
    }
}
