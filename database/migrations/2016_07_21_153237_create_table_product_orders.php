<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProductOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productOrders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userId')->index()->unsigned()->comment('ID пользователя');
            $table->integer('shopId')->index()->unsigned()->comment('ID магазина');
            $table->integer('status')->unsigned()->default(0)->comment('Статус');
            $table->integer('count')->unsigned()->default(0)->comment('Количество позиций');
            $table->integer('cost')->unsigned()->default(0)->comment('Суммарная стоимость');
            $table->string('description')->nullable()->comment('Описание');
            $table->timestamp('createdAt')->nullable();
            $table->timestamp('updatedAt')->nullable();
            $table->timestamp('deletedAt')->nullable();

            $table->foreign('userId')->references('id')->on('users');
            $table->foreign('shopId')->references('id')->on('shops');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('productOrders');
    }
}
