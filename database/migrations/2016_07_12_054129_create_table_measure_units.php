<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMeasureUnits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('measureUnits', function (Blueprint $table) {
            $table->char('id', 8)->unique()->comment('Строковый ID (международное обозначение)');
            $table->char('categoryId', 2)->index()->comment('ID группы измерений');
            $table->string('name')->index()->comment('Название');
            $table->char('designation', 8)->index()->comment('Обозначение (национальное)');
            $table->char('code', 3)->nullable()->comment('Международный числовой код');
            $table->char('codeStr', 3)->nullable()->comment('Международный строковый код');
            $table->char('parentId', 8)->nullable()->index()->comment('Базовая единица измерения');
            $table->float('ratio', 8)->nullable()->comment('Коэффициент отношения с базовой единицей измерения');
            $table->smallInteger('order')->unsigned()->nullable()->index()->default(0)->comment('Порядок сортировки');
            $table->timestamp('deletedAt')->nullable();

            $table->primary('id');
            $table->foreign('categoryId')->references('id')->on('measureCategories');
            $table->foreign('parentId')->references('id')->on('measureUnits')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('measureUnits');
    }
}
