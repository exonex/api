<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProductCompanies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productCompanies', function (Blueprint $table) {
            $table->increments('id');
            $table->char('countryId', 2)->index()->comment('ID страны');
            $table->string('name')->index()->comment('Название');
            $table->string('nameEn')->index()->comment('Английское международное название');
            $table->string('description')->nullable()->comment('Описание');
            $table->string('logo')->nullable()->comment('Логотип');
            $table->string('site')->nullable()->comment('Сайт');
            $table->integer('order')->unsigned()->nullable()->index()->default(0)->comment('Порядок сортировки');
            $table->timestamp('createdAt')->nullable();
            $table->timestamp('updatedAt')->nullable();
            $table->timestamp('deletedAt')->nullable();

            $table->foreign('countryId')->references('id')->on('countries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('productCompanies');
    }
}
