<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Models\LogApiCallRule;

class CreateTableLogApiCallRules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logApiCallRules', function (Blueprint $table) {
            $table->increments('id');
            $table->string('userId', 16)->default(LogApiCallRule::ANY)->index()->comment('ID пользователя');
            $table->string('type', 2)->default(LogApiCallRule::ANY)->index()->comment('Тип записи - реквест/респонс RQ/RS');
            $table->string('code', 32)->default(LogApiCallRule::ANY)->index()->comment('Метод реквеста или код респонса');
            $table->string('action', 255)->default(LogApiCallRule::ANY)->index()->comment('Выполняемый метод');
            $table->string('tag', 32)->default(LogApiCallRule::ANY)->index()->comment('Тэг запроса/составного действия');
            $table->string('ip', 15)->default(LogApiCallRule::ANY)->index()->comment('IP запроса');
            $table->tinyInteger('save')->unsigned()->default(0)->comment('Флаг сохранения записи лога');
            $table->string('comment', 255)->nullable()->comment('Комментарий к правилу');
            $table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updatedAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('deletedAt')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logApiCallRules');
    }
}
