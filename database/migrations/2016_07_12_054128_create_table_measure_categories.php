<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMeasureCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('measureCategories', function (Blueprint $table) {
            $table->char('id', 2)->unique()->comment('Строковый ID (обозначение)');
            $table->string('name')->unique()->comment('Название группы');
            $table->smallInteger('order')->unsigned()->nullable()->default(0)->comment('Порядок сортировки');

            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('measureCategories');
    }
}
