<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\ProductModel;

class CreateTableProductModels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productModels', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->index()->comment('Название');
            $table->string('code')->index()->nullable()->comment('Код');
            $table->integer('categoryId')->index()->unsigned()->comment('ID категории товаров');
            $table->integer('brandId')->index()->unsigned()->comment('ID марки');
            // Добавляем поля параметров. Пока по 32 каждого вида
            $colLimit = ProductModel::PROPERTIES_COLS_LIMIT;
            for ($i = 0; $i < $colLimit; $i++) {
                $table->integer("i$i")->index()->nullable();
            }
            for ($i = 0; $i < $colLimit; $i++) {
                $table->float("f$i")->index()->nullable();
            }
            for ($i = 0; $i < $colLimit; $i++) {
                $table->tinyInteger("b$i")->index()->unsigned()->nullable();
            }
            for ($i = 0; $i < $colLimit; $i++) {
                $table->string("s$i")->index();
            }
            $table->jsonb('properties')->index();
            $table->timestamp('createdAt')->nullable();
            $table->timestamp('updatedAt')->nullable();
            $table->timestamp('deletedAt')->nullable();

            $table->foreign('categoryId')->references('id')->on('productCategories');
            $table->foreign('brandId')->references('id')->on('productBrands');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('productModels');
    }
}
