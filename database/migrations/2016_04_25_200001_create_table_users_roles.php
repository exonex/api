<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use \App\Models\UserRole;

class CreateTableUsersRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists(UserRole::USERS_PIVOT_TABLE);
        Schema::create(UserRole::USERS_PIVOT_TABLE, function (Blueprint $table) {
            $table->integer('userId')->unsigned()->index()->comment('ID пользователя');
            $table->char('roleId', 3)->unsigned()->index()->comment('ID роли');
            $table->timestamp('createdAt')->nullable();
            $table->timestamp('updatedAt')->nullable();
            $table->timestamp('deletedAt')->nullable();

            $table->primary(['userId', 'roleId'], 'usersRolesPK');
            $table->foreign('userId')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('roleId')->references('id')->on('userRoles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(UserRole::USERS_PIVOT_TABLE);
    }
}
