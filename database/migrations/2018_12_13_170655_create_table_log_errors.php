<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLogErrors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logErrors', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamp('time')->index()->comment('Время записи');
            $table->integer('userId')->nullable()->index()->comment('ID пользователя, под которым произошла ошибка');
            $table->string('action', 255)->nullable()->index()->comment('Выполняемое действие/метод');
            $table->integer('requestId')->nullable()->comment('ID лога запроса');
            $table->char('type', 32)->index()->comment('Тип события/записи');
            $table->mediumText('message')->nullable()->comment('Сообщение ошибки');
            $table->longText('data')->nullable()->comment('Дополнительные данные');
            $table->unsignedInteger('size')->default(0)->comment('Размер данных (в символах)');
            $table->string('file', 255)->nullable()->comment('Файл ошибки');
            $table->unsignedSmallInteger('line')->nullable()->comment('Строка ошибки');
            $table->longText('trace')->nullable()->comment('Трэйс ошибки');
            $table->ipAddress('ip')->nullable()->comment('IP запроса');

            $table->foreign('requestId')->references('id')->on('logApiCalls');
            $table->foreign('userId')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logErrors');
    }
}
