<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('users');
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->index()->comment('Имя и отчество');
            $table->string('lastName')->index()->default('')->comment('Фамилия');

            $table->string('nickname')->index()->default('')->comment('Ник');
            $table->char('gender', 1)->unsigned()->default('')->comment("Пол: '' - не указан, 'm' - м, 'f' - ж");
            $table->date('birthdate')->nullable()->comment('Дата рождения YYYY-MM-DD');
            $table->string('avatar')->nullable()->comment('Адрес файла с аватаркой');

            $table->char('phone', 20)->nullable()->comment('Сотовый телефон');
            $table->string('email')->unique()->comment('E-mail');
            $table->string('password')->index()->comment('Хэш пароля');

            $table->char('countryId', 2)->nullable()->index()->comment('Строковый ID страны');
            $table->integer('stateId')->nullable()->index()->unsigned()->comment('ID региона');
            $table->string('city')->nullable()->comment('Город (населённый пункт)');
            $table->jsonb('settings')->default('[]')->comment('Дополнительные параметры пользователя');

            $table->char('rememberToken', 100)->nullable();
            $table->timestamp('createdAt')->nullable();
            $table->timestamp('updatedAt')->nullable();
            $table->timestamp('deletedAt')->nullable();

            $table->foreign('countryId')->references('id')->on('countries');
            $table->foreign('stateId')->references('id')->on('states');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
