<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCountries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('countries');
        Schema::create('countries', function (Blueprint $table) {
            $table->char('id', 2)->index()->comment('Строковый ID Alpha2');
            $table->char('id3', 3)->unique()->comment('Код Alpha3');
            $table->string('name')->unique()->comment('Название');
            $table->string('fullName')->unique()->comment('Полное официальное название');
            $table->string('nameEn')->unique()->comment('Название на английском языке');
            $table->char('code', 3)->index()->comment('Международный числовой код');
            $table->char('phoneCode', 8)->index()->comment('Междунароный телефонный код');
            $table->char('domain', 2)->index()->nullable()->comment('Доменная зона');
            $table->smallInteger('order')->index()->unsigned()->default(0)->comment('Порядок сортировки');
            $table->timestamp('deletedAt')->nullable();

            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
