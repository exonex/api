<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProductBrands extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productBrands', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('companyId')->index()->unsigned()->comment('ID компании');
            $table->string('name')->index()->comment('Название');
            $table->string('nameEn')->index()->comment('Английское международное название');
            $table->string('description')->nullable()->comment('Описание');
            $table->string('logo')->nullable()->comment('Логотип');
            $table->integer('order')->unsigned()->nullable()->index()->default(0)->comment('Порядок сортировки');
            $table->timestamp('createdAt')->nullable();
            $table->timestamp('updatedAt')->nullable();
            $table->timestamp('deletedAt')->nullable();

            $table->foreign('companyId')->references('id')->on('productCompanies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('productBrands');
    }
}
