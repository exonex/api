<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUserRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('userRoles');
        Schema::create('userRoles', function (Blueprint $table) {
            $table->char('id', 3)->unique()->comment('Строковый ID');
            $table->string('name')->unique()->comment('Название');
            $table->string('description')->nullable()->comment('Описание');
            $table->smallInteger('order')->unsigned()->default(0)->comment('Порядок сортировки');
            $table->timestamp('createdAt')->nullable();
            $table->timestamp('updatedAt')->nullable();
            $table->timestamp('deletedAt')->nullable();

            $table->primary('id', 'userRolesPK');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('userRoles');
    }
}
