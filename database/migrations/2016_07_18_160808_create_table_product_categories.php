<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProductCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productCategories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->index()->comment('Название категории');
            $table->string('pathName')->index()->comment('Список родительских категорий, разделённый /');
            $table->string('path')->index()->nullable()->comment('Список ID родителей, разделённый /');
            $table->string('description')->nullable()->comment('Описание');
            $table->jsonb('measures')->index()->nullable()->default('[]')->comment('Характеристики модели');
            // These columns are needed for Baum's Nested Set implementation to work.
            $table->integer('parentId')->index()->unsigned()->nullable()->comment('Родительская категория');
            $table->integer('nsLeft')->index()->nullable()->unsigned()->comment('Поле для nested set');
            $table->integer('nsRight')->index()->nullable()->unsigned()->comment('Поле для nested set');
            $table->integer('nsDepth')->unsigned()->default(0)->comment('Поле для nested set');
            // Остальные поля
            $table->integer('order')->index()->unsigned()->default(0)->comment('Порядок сортировки');
            $table->timestamp('createdAt')->nullable();
            $table->timestamp('updatedAt')->nullable();
            $table->timestamp('deletedAt')->nullable();

            $table->unique(['name', 'parentId']);
            $table->foreign('parentId')->references('id')->on('productCategories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('productCategories');
    }
}
