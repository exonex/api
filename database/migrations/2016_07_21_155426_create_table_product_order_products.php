<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProductOrderProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productOrderProducts', function (Blueprint $table) {
            $table->integer('productId')->index()->unsigned()->comment('ID товара');
            $table->integer('orderId')->index()->unsigned()->comment('ID заказа');
            $table->integer('count')->unsigned()->comment('Количество товара в позиции');
            $table->timestamp('createdAt')->nullable();
            $table->timestamp('updatedAt')->nullable();

            $table->primary(['productId', 'orderId'], 'productOrderProductsPK');
            $table->foreign('productId')->references('id')->on('products');
            $table->foreign('orderId')->references('id')->on('productOrders');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('productOrderProducts');
    }
}
