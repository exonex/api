<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUserToken extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('userTokens');
        Schema::create('userTokens', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->index()->comment('Почта пользователя');
            $table->string('token')->unique()->comment('Токен');
            $table->tinyInteger('type')->default(0)->comment('Тип токена: 0 - сброс пароля, 1 - активация пользователя, 2 - смена email');
            $table->timestamp('createdAt')->nullable();
            $table->timestamp('updatedAt')->nullable();

            $table->foreign('email')->references('email')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('userTokens');
    }
}
