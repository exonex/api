<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLogApiCalls extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logApiCalls', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamp('time')->index()->comment('Время записи');
            $table->integer('userId')->nullable()->index()->comment('ID пользователя');
            $table->char('type', 2)->index()->comment('Тип записи - реквест/респонс RQ/RS');
            $table->string('code', 32)->nullable()->comment('Метод реквеста или код респонса');
            $table->string('action', 255)->nullable()->index()->comment('Выполняемый метод');
            $table->string('tag', 32)->nullable()->index()->comment('Тэг запроса/составного действия');
            $table->integer('requestId')->nullable()->comment('ID лога запроса (для распонсов)');
            $table->longText('data')->nullable()->comment('Дополнительные данные');
            $table->unsignedInteger('size')->default(0)->comment('Размер данных (в символах)');
            $table->float('runtime')->nullable()->comment('Время ответа в секундах (для респонсов)');
            $table->ipAddress('ip')->nullable()->comment('IP запроса');

            $table->foreign('requestId')->references('id')->on('logApiCalls');
            $table->foreign('userId')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logApiCalls');
    }
}
