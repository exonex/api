<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableShops extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shops', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->index()->comment('Название');
            $table->string('description')->nullable()->comment('Описание');
            $table->char('countryId', 2)->index()->comment('ID страны');
            $table->integer('stateId')->index()->unsigned()->comment('ID региона');
            $table->string('address')->index()->comment('Адрес');
            $table->jsonb('contacts')->index()->default('[]')->comment('Контакты магазина');
            $table->timestamp('createdAt')->nullable();
            $table->timestamp('updatedAt')->nullable();
            $table->timestamp('deletedAt')->nullable();

            $table->unique(['name', 'countryId', 'stateId']);
            $table->foreign('countryId')->references('id')->on('countries');
            $table->foreign('stateId')->references('id')->on('states');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('shops');
    }
}
