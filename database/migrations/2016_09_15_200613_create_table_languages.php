<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLanguages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('languages', function (Blueprint $table) {
            $table->char('id', 2)->index()->comment('Строковый ID Alpha2 (ISO 639-1)');
            $table->string('name')->unique()->comment('Название');
            $table->string('nameEn')->unique()->comment('Название на английском языке');
            $table->char('iso2', 3)->unique()->comment('Код ISO 639-2');
            $table->char('iso3', 3)->unique()->comment('Код ISO 639-3');
            $table->char('gost', 3)->unique()->comment('Код ГОСТ 7.75–97');
            $table->char('code', 3)->index()->nullable()->comment('Международный числовой код');
            $table->integer('order')->index()->unsigned()->default(0)->comment('Порядок сортировки');
            $table->timestamp('deletedAt')->nullable();

            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('languages');
    }
}
