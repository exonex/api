<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
*/

Route::pattern('crudAction', 'list|read|save|delete');

// API routes group
Route::group(['prefix' => 'v1', 'as' => 'api.', 'namespace' => 'API', 'middleware' => 'auth:api'], function(){
    Route::get('/status', ['as' => 'status', 'uses' => 'BaseController@getStatus']);

    // Static data, like countries, cities, dictionaries and etc.
    Route::group(['prefix' => 'static', 'as' => 'static.'], function(){
        Route::get('/countries', ['as' => 'countries', 'uses' => 'BaseController@getCountries']);
    });
});