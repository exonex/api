<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
*/

// Common app pages
Route::group(['as' => 'pages.', 'namespace' => 'Web'], function () {
    // Controller-access (for example)
    Route::get('/', ['as' => 'index', 'uses' => 'PagesController@getIndex']);

    // Easy view-access is better for stable static pages
    Route::view('/index', 'pages.index')->name('index');
    Route::view('/contacts', 'pages.contacts')->name('contacts');

    // Protected pages
    Route::group(['middleware' => 'auth'], function () {

    });
});

// Authorization
Route::group(['as' => 'auth.', 'namespace' => 'Web'], function () {
    // Web authorization
    Route::get('/login',  ['as' => 'login',  'uses' => 'AuthController@getLogin',  'middleware' => 'guest']);
    Route::post('/login', ['as' => 'log-in', 'uses' => 'AuthController@postLogin', 'middleware' => 'guest']);
    Route::get('/logout', ['as' => 'logout', 'uses' => 'AuthController@getLogout', 'middleware' => 'auth']);
});

// Control panel
Route::group(['prefix' => 'cp', 'as' => 'cp.', 'namespace' => 'ControlPanel', 'middleware' => 'cp'], function () {
    Route::get('/',  ['as' => 'index', 'uses' => 'BaseController@getIndex']);
});

// User
Route::group(['prefix' => 'user', 'as' => 'user.', 'namespace' => 'Web', 'middleware' => 'auth'], function () {
    Route::get('/profile', ['as' => 'profile', 'uses' => 'UserController@getProfile']);
    Route::get('/oauth',   ['as' => 'oauth',   'uses' => 'UserController@getOauth']);
});